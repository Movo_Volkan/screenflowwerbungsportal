<?php


namespace App\util;


class AppConfigs
{
    public static  $APP_STAGE_DEV = "dev";
    public static  $APP_STAGE_TEST = "test";
    public static $APP_STAGE_PROD = "prod";

    public static $APP_URL = "https://app.screenflow24.com";

    private static $TAX_RATE = "19";
    public static $TAX_RATE_16 = "16";
    public static $TAX_RATE_19 = "19";

    public static function getAppURL(){
        return self::$APP_URL;
    }

    public static function getTextRate(){
        return self::$TAX_RATE;
    }
}