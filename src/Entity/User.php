<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $salutation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondname;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $profilePicture;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $registerCode;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $passwordResetCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordResetCodeTime;

    /**
     * @ORM\Column(name="created", type="datetime",nullable=true)
     */
    private $created;

    /**
     * @ORM\Column(type="boolean")
     */
    private $firstLogin;

    /**
     * @ORM\OneToMany(targetEntity=Media::class, mappedBy="user")
     */
    private $media;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    public function __construct()
    {
        $this->media = new ArrayCollection();
        $this->isActive = true;
        $this->players = new ArrayCollection();
    }

    public function eraseCredentials()
    {
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }


    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalutation(): ?string
    {
        return $this->salutation;
    }

    public function setSalutation(?string $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getSecondName(): ?string
    {
        return $this->secondname;
    }

    public function setSecondName(string $secondName): self
    {
        $this->secondname = $secondName;

        return $this;
    }

    public function getProfilePicture()
    {
        if($this->profilePicture != null)
        {
            return stream_get_contents($this->profilePicture);
        }
        return null;
        
    }

    public function setProfilePicture($profilePicture): self
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    public function getRegisterCode(): ?string
    {
        return $this->registerCode;
    }

    public function setRegisterCode(?string $registerCode): self
    {
        $this->registerCode = $registerCode;

        return $this;
    }

    public function getActivate(): ?bool
    {
        return $this->activate;
    }

    public function setActivate(bool $activate): self
    {
        $this->activate = $activate;

        return $this;
    }

    public function getPasswordResetCode(): ?string
    {
        return $this->passwordResetCode;
    }

    public function setPasswordResetCode(?string $passwordResetCode): self
    {
        $this->passwordResetCode = $passwordResetCode;

        return $this;
    }

    public function getPasswordResetCodeTime(): ?\DateTimeInterface
    {
        return $this->passwordResetCodeTime;
    }

    public function setPasswordResetCodeTime(?\DateTimeInterface $passwordResetCodeTime): self
    {
        $this->passwordResetCodeTime = $passwordResetCodeTime;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }


    public function getFirstLogin(): ?bool
    {
        return $this->firstLogin;
    }

    public function setFirstLogin(bool $firstLogin): self
    {
        $this->firstLogin = $firstLogin;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * @return Collection|Media[]
     */
    public function getMedia(): Collection
    {
        return $this->media;
    }

    public function getVideoMedia(): Collection
    {
        return $this->media->filter(function(Media $media){
            return $media->getType() == Media::$MEDIA_TYP_VIDEO;
        });
    }

    public function getImageMedia(): Collection
    {
       return $this->media->filter(function(Media $media){
            return $media->getType() == Media::$MEDIA_TYP_IMAGE;
        });
    }

    public function addMedium(Media $medium): self
    {
        if (!$this->media->contains($medium)) {
            $this->media[] = $medium;
            $medium->setUser($this);
        }

        return $this;
    }

    public function removeMedium(Media $medium): self
    {
        if ($this->media->contains($medium)) {
            $this->media->removeElement($medium);
            // set the owning side to null (unless already changed)
            if ($medium->getUser() === $this) {
                $medium->setUser(null);
            }
        }

        return $this;
    }

    public function getUsedStorageInBytes()
    {
        $usedBytes = 0;
        $mediaList = $this->getMedia();

        foreach ($mediaList as $media)
        {
            $usedBytes = $usedBytes + $media->getSize();
        }
        return $usedBytes;
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }
}
