<?php


namespace App\Controller\Base;
use App\util\Utilities;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;


class EMailController extends BaseController
{

    const DEFAULT_EMAIL = "contact@movosolutions.de" ;
    private  $DEFAULT_EMAIL = "contact@movosolutions.de";
    private $mailer = null;
    private $email = null;

    function __construct(MailerInterface $mailer,LoggerInterface $logger) {
        parent::__construct($logger);
        $this->mailer = $mailer;
    }

    function createTemplatedEmail($from,$fromName,$to,$subject,$templatePath,$context = [])
    {
        if($from == null || $from == "")
        {
            $from = static::DEFAULT_EMAIL;
        }
        if ($fromName == null || $fromName == "")
        {
            $fromName = static::DEFAULT_EMAIL;
        }

        $this->email = (new TemplatedEmail())
        ->from(new Address($from, $fromName))
        ->to($to)
        ->subject($subject)
        ->htmlTemplate($templatePath)
        ->context($context);
    }

    function sendEmail()
    {
        if($this->email != null)
        {
            $this->mailer->send($this->email);
        }
        else
        {
            return Utilities::createErrorResponse($this->getLanguageStringValue("Create a Email."));
        }

    }
}