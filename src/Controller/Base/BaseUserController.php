<?php 
namespace App\Controller\Base;

use App\Controller\Subscription\SubscriptionController;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BaseUserController extends BaseController
{
    private HttpClientInterface $httpClientInterface;

    function __construct(HttpClientInterface $httpClientInterface,LoggerInterface $logger) {
        parent::__construct($logger);
        $this->httpClientInterface = $httpClientInterface;
    }

    public function renderTemplate($template,$attributesArray = [])
    {
        try {
            $this->refreshSubscriptionInformation();
            $userAttributes = array_merge([
            ],$attributesArray);
            return parent::renderTemplate($template,$userAttributes);
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
        }
    }

   private function refreshSubscriptionInformation()
   {
       try {
           if($this->getUser() != null)
           {
               $subscriptionController = new SubscriptionController($this->httpClientInterface,$this->getLogger());
               $subscriptionController->refreshSubscriptionInformation($this->getUser(),$this->getDoctrine()->getManager());
           }
           else
           {
               $this->getLogger()->info("No logged in user was found. Possibly a public controller method was called.");
           }

       }
       catch (\Exception $e)
       {
           $this->getLogger()->error($e);
       }
   }
}