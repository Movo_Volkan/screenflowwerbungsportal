<?php 
namespace App\Controller\Base;
use App\util\Utilities;
use App\util\AppConfigs;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\SimpleXMLElement;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;

class BaseController extends AbstractController implements iController
{
    private LoggerInterface $logger;

    function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    protected function renderTemplate($template,$attributesArray)
    {
        try {
            return $this->render($template, array_merge([
                'CurrentYear' => date("Y"),
                'CurrentLanguage' => 'DE' ,
                'LanguageStrings' => Utilities::buildLanguageStrings(),
                'LanguageStringsJs' => Utilities::buildLanguageStringsJs(),
                'IsRegistrationActive' => $this->getAppRegister(),
                'AppVersion'=> $this->getAppVersion(),
                'AppStage'=> $this->getAppStage()
            ],$attributesArray));
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
        }
    }

    protected function render404($attributesArray = [])
    {
        try {
            return $this->render("404.html.twig", array_merge([
                'CurrentYear' => date("Y"),
                'CurrentLanguage' => 'DE' ,
                'LanguageStrings' => Utilities::buildLanguageStrings(),
                'LanguageStringsJs' => Utilities::buildLanguageStringsJs(),
                'IsRegistrationActive' => $this->getAppRegister()
            ],$attributesArray));
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
        }
    }

    protected function getPublicRootPath()
    {
        return $this->getParameter('kernel.project_dir') . '/public/';;
    }

    protected function getLanguageStringValue($key)
    {
        return Utilities::buildLanguageStrings()[$key];
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function getAppStage() {
        return $this->getParameter('app.stage');
    }

    private function getAppRegister() {
        return $this->getParameter('app.register');
    }

    private function getAppVersion() {
        return $this->getParameter('app.version');
    }

    public function getAppDomain(){
        return $this->getParameter('app.domain');
    }
}