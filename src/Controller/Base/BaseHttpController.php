<?php


namespace App\Controller\Base;


use Doctrine\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BaseHttpController
{

    private HttpClientInterface $httpClientInterface;
    private LoggerInterface $logger;

    function __construct(HttpClientInterface $httpClientInterface,LoggerInterface $logger) {
        $this->httpClientInterface = $httpClientInterface;
        $this->logger = $logger;
    }

    /**
     * @return HttpClientInterface
     */
    protected function getHttpClientInterface(): HttpClientInterface
    {
        return $this->httpClientInterface;
    }

    protected function get($url,$headers,$body=[])
    {
        try {
            $response = $this->getHttpClientInterface()->request('GET', $url, [
                'headers' => $headers,
                'body' => $body
            ]);
            return json_decode($response->getContent());
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
            return null;
        }
    }

    protected function post($url,$headers,$body=[])
    {
        try {
            $response = $this->httpClientInterface->request('POST', $url,
                [
                    'headers' => $headers,
                    'body' => $body
                ]);

            return json_decode($response->getContent());
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
            return null;
        }
    }

    protected function delete($url,$headers,$body=[])
    {
        try {
            $response = $this->httpClientInterface->request('DELETE', $url,
                [
                    'headers' => $headers,
                    'body' => $body
                ]);

            return json_decode($response->getContent());
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
            return null;
        }
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}