<?php


namespace App\Controller\Base;


use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BaseServiceController implements iController
{
    private $logger;
    private $client;

    private $appStage;

    function __construct(HttpClientInterface $httpClientInterface,LoggerInterface $logger, $appStage = "dev") {
        $this->logger = $logger;
        $this->client = $httpClientInterface;
        $this->appStage = $appStage;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return HttpClientInterface
     */
    public function getClient(): HttpClientInterface
    {
        return $this->client;
    }

    function getAppStage()
    {
        return $this->appStage;
    }
}