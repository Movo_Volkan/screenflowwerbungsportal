<?php

namespace App\Controller\Media;
use App\Controller\Service\AWSController;
use App\Entity\Media;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Base\BaseUserController;
use Symfony\Component\HttpFoundation\Request;
use App\util\Utilities;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MediaController extends BaseUserController
{

    /**
     * @Route("/upload")
     * @IsGranted("ROLE_USER")
     */
    public function upload()
    {
        try
        {
            $breadcrumbPath = array();
            $breadcrumbPath[]  = array(
                "Url" => "/upload",
                "Name" => $this->getLanguageStringValue("uploadMedia")
            );
            return $this->renderTemplate('/media/upload.html.twig', [
                'SiteName' => $this->getLanguageStringValue("uploadMedia"),
                'NavName' => 'Upload',
                'UsedStorageByte' => $this->getUser()->getUsedStorageInBytes(),
                'BreadcrumbPath' => $breadcrumbPath
            ]);
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/videos", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function videos(Request $request)
    {
        try {
            if($request->get("output") == "json")
            {
                $media = $this->getDoctrine()->getRepository(Media::class)->findBy(["type"=>Media::$MEDIA_TYP_VIDEO, "user"=>$this->getUser()]);
                $inserts = ["inserts" => []];
                foreach ($media as $medium)
                {
                    $inserts["inserts"][] = (object) [
                        'id' => $medium->getId(),
                        'src' => $medium->getPath(),
                        'thumbnail' => $medium->getThumbnail(),
                        'type' => $medium->getType(),
                    ];
                }
                return new JsonResponse($inserts);
            }
            else
            {
                $breadcrumbPath = array();
                $breadcrumbPath[]  = array(
                    "Url" => "/videos",
                    "Name" => $this->getLanguageStringValue("videos")
                );
                return $this->renderTemplate('/media/videos/videos.html.twig', [
                    'SiteName' => $this->getLanguageStringValue("videos"),
                    'NavName' => 'Videos',
                    'UsedStorageByte' => $this->getUser()->getUsedStorageInBytes(),
                    'BreadcrumbPath' => $breadcrumbPath
                ]);
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/images" ,methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function images(Request $request)
    {
        try {
            if($request->get("output") == "json")
            {
                $media = $this->getDoctrine()->getRepository(Media::class)->findBy(["type"=>Media::$MEDIA_TYP_IMAGE, "user"=>$this->getUser()]);
                $inserts = ["inserts" => []];
                foreach ($media as $medium)
                {
                    $inserts["inserts"][] = (object) [
                        'id' => $medium->getId(),
                        'src' => $medium->getPath(),
                        'thumbnail' => $medium->getThumbnail(),
                        'type' => $medium->getType(),
                    ];
                }
                return new JsonResponse($inserts);
            }
            else
            {
                $breadcrumbPath = array();
                $breadcrumbPath[]  = array(
                    "Url" => "/images",
                    "Name" => $this->getLanguageStringValue("pictures")
                );
                return $this->renderTemplate('/media/images/images.html.twig', [
                    'SiteName' => $this->getLanguageStringValue("pictures"),
                    'NavName' => 'Images',
                    'UsedStorageByte' => $this->getUser()->getUsedStorageInBytes(),
                    'BreadcrumbPath' => $breadcrumbPath
                ]);
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/uploadmedia")
     * @IsGranted("ROLE_USER")
     */
    public function media(Request $request,HttpClientInterface $httpClientInterface,LoggerInterface $logger)
    {
        try {
            $file = $request->files->get('files')[0];
            $category = $request->get('category');
            $tags = $request->get('tags');
            $ageCategory = $request->get('ageCategory');
            $gender = $request->get('gender');
            $filePath = $file->getPathname();
            $fileSize = $file->getSize();
            $tempFile = $file->getFileInfo();
            $mimetype = $file->getMimeType();
            $extension = $file->getClientOriginalExtension();
            $fileOriginalName = $file->getClientOriginalName();
            $uniqFileName = uniqid();


            $base64 = "";
            $mediaType = "";
            if (strpos($file->getMimeType(), "image") !== false) {
                $storeFolder = "uploads" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . $this->getUser()->getId();
                $imageBlob = Utilities::scaleImageFileToBlob($filePath);
                $base64 = 'data:image/' . $extension . ';base64,' . base64_encode($imageBlob);
                $mediaType = "image";
            } else {
                $storeFolder = "uploads" . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR . $this->getUser()->getId();
                $mediaType = "video";
            }

            $mediaFilePath = dirname($_SERVER['DOCUMENT_ROOT'], 1) . DIRECTORY_SEPARATOR . 'mediaFiles' . DIRECTORY_SEPARATOR . $storeFolder;

            $filesystem = new Filesystem();

            if(!$filesystem->exists($mediaFilePath))
            {
                $filesystem->mkdir($mediaFilePath);
            }

            $stream = fopen($filePath, "r");

            $filesystem->dumpFile($mediaFilePath . DIRECTORY_SEPARATOR . $fileOriginalName, stream_get_contents($stream));

            fclose($stream);

            $media = new Media();
            $media->setUser($this->getUser())
                ->setName($fileOriginalName)
                ->setPath($mediaFilePath . DIRECTORY_SEPARATOR . $fileOriginalName)
                ->setType($mediaType)
                ->setThumbnail($base64)
                ->setSize($fileSize)
                ->setUniqueFileName($uniqFileName)
                ->setMediaCategory($category)
                ->setTags($tags)
                ->setAgeCategory($ageCategory)
                ->setGender($gender)
                ->setCreated(new \DateTime());

            $this->getUser()->addMedium($media);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($media);
            $entityManager->flush();

            return Utilities::createSuccessfulResponse($this->getLanguageStringValue("pictureUploaded"),200,['id'=>$media->getId()]);
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error($e);
            return Utilities::createErrorResponse($e->getMessage());
        }

    }

    /**
     * @Route("/setthumbnail")
     * @IsGranted("ROLE_USER")
     */
    public function setThumbnail(Request $request) {
        try {
            $mediaId = $request->request->get('MediaId');
            $thumbnail = $request->request->get('Thumbnail');
            $media = $this->getDoctrine()->getRepository(Media::class)->getMediaById($mediaId,$this->getUser());
            $media->setThumbnail($thumbnail);
            $this->getDoctrine()->getManager()->flush();
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/removeMedium")
     * @IsGranted("ROLE_USER")
     */
    public function removeMedium(Request $request,HttpClientInterface $httpClientInterface,LoggerInterface $logger){
        try {
            $mediaId = $request->get("MediaId");
            $mediaType = $request->get("MediaType");

            if($mediaId != null && $mediaType != null){
                $media = $this->getDoctrine()->getRepository(Media::class)->findOneBy(["id"=>$mediaId]);
                if($media != null)
                {
                    $this->getDoctrine()->getManager()->remove($media);
                    $awsController = new AWSController($httpClientInterface,$logger,$this->getAppStage());
                    $awsController->deleteMedia($media->getPath());
                    $this->getDoctrine()->getManager()->flush();
                    return Utilities::createSuccessfulResponse($this->getLanguageStringValue("mediaDeleted"));
                }
            }
            return Utilities::createErrorResponse($this->getLanguageStringValue("errorContactAdmin"));
        }
        catch (Exception $e)
        {
            $this->getLogger()->error($e);
            return Utilities::createErrorResponse($this->getLanguageStringValue("cantDeleteMedia"));
        }
    }

    /**
     * @Route("/editMedium")
     * @IsGranted("ROLE_USER")
     */
    public function editMedium(Request $request) {
        try {
            $mediaId = $request->get("MediaId");
            $mediaName = $request->get("MediaName");
            $mediaDescription = $request->get("MediaDescription");
            $mediaType = $request->get("MediaType");

            if($mediaId != null && $mediaType != null && $mediaName != null){
                $media = $this->getDoctrine()->getRepository(Media::class)->findOneBy(["id"=>$mediaId]);
                if($media != null)
                {
                    $media->setName($mediaName)
                        ->setDescription($mediaDescription);
                    $this->getDoctrine()->getManager()->flush();
                    return Utilities::createSuccessfulResponse($this->getLanguageStringValue("mediaSaved"));
                }
            }
            return Utilities::createErrorResponse($this->getLanguageStringValue("errorContactAdmin"));
        }
        catch (Exception $e)
        {
            $this->getLogger()->error($e);
        }
    }
}