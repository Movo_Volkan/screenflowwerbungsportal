<?php

namespace App\Controller\Dashboard;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Base\BaseUserController;

class DashboardController extends BaseUserController
{
    /**
     * @Route("/")
     * @IsGranted("ROLE_USER")
     */
    public function init()
    {
        try {
            $breadcrumbPath = array();
            $breadcrumbPath[]  = array(
                "Url" => "/",
                "Name" => $this->getLanguageStringValue("dashboard")
            );
            return $this->renderTemplate('index.html.twig',[
                'SiteName' => 'Dashboard',
                'NavName' => 'Home',
                'BreadcrumbPath' => $breadcrumbPath
            ]);
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }
}