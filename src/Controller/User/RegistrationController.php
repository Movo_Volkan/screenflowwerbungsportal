<?php

// src/Controller/RegistrationController.php
namespace App\Controller\User;

use App\Controller\Base\BaseController;
use App\Controller\Base\EMailController;
use App\Form\UserType;
use App\Entity\User;
use App\util\AppConfigs;
use App\util\Utilities;
use Exception;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use OAuth2\OAuth2;
use phpDocumentor\Reflection\Types\This;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RegistrationController extends BaseController
{
    /**
     * @Route("/register", name="user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder,MailerInterface $mailer,LoggerInterface $logger)
    {
        try {
            // Get Symfony to interface with this existing session
            $session = new Session(new PhpBridgeSessionStorage());
            $session->start();

            // 1) build the form
            $user = new User();
            $form = $this->createForm(UserType::class, $user);


            // 2) handle the submit (will only happen on POST)
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {


                if($this->getDoctrine()->getRepository(User::class)->count(["email"=>$user->getEmail()]) > 0)
                {
                    return $this->render404();
                }

                // 3) Encode the password (you could also do this via Doctrine listener)
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $user->setRegisterCode(Utilities::UuidV1());
                $user->setFirstLogin(true);
                $user->setActivate(false);
                $user->setCreated(new \DateTime());

                // 4) save the User!
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                if($this->getAppStage() == AppConfigs::$APP_STAGE_PROD || $this->getAppStage() == AppConfigs::$APP_STAGE_TEST) {
                    $emailController = new EMailController($mailer,$logger);
                    $emailController->createTemplatedEmail('service@movods.com',
                        $this->getLanguageStringValue("MovoDSRegistration"),
                        $user->getEmail(),
                        $this->getLanguageStringValue("MovoDSRegistration"),
                        'emails/activationEmail.html.twig',
                        [
                            'LanguageStrings' => Utilities::buildLanguageStrings(),
                            'RegisterCode' => $user->getRegisterCode(),
                            'CurrentYear' => date("Y")
                        ]);

                    $emailController->sendEmail();
                }

                return $this->redirect('/login?registerSuccess=true');
            }

            return $this->renderTemplate('registration/register.html.twig',['form' => $form->createView()]);
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/register/activateAccount")
     */
    public function activateAccount(Request $request, ClientManagerInterface $clientManager ,HttpClientInterface $httpClientInterface,LoggerInterface $logger)
    {
        try {
            $registerCode = $request->query->get('RegisterCode');
            if($registerCode != null)
            {
                $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["registerCode"=>$registerCode]);
                if ($user != null)
                {
                    $user->setActivate(true);
                    $user->setRegisterCode(null);

                    $stripeCustomerController = new StripeCustomerController($httpClientInterface,$logger);
                    $customer = $stripeCustomerController->createCustomer($user->getFirstname(),$user->getSecondName(),$user->getEmail());
                    $user->setStripeCustomerId($customer->id);

                    $subscription = new Subscription();
                    $subscription->setStartDateTime(Utilities::getDateTimeNow());
                    $subscription->setEndDateTime(Utilities::getDateTimeNow()->modify('+7 day'));
                    $subscription->setPlayerQuantity(1);
                    $subscription->setTrial(1);
                    $subscription->setStorage(5);
                    $subscription->setPlanName("Trial");
                    $this->getDoctrine()->getManager()->persist($subscription);
                    $user->setSubscription($subscription);
                    $this->getDoctrine()->getManager()->flush();

                    $client = $clientManager->createClient();
                    $client->setAllowedGrantTypes(array(OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS));
                    $user->setOAuthClient($client);

                    $this->getDoctrine()->getManager()->flush();

                    $clientManager->updateClient($client);

                    return $this->renderTemplate('registration/activateAccount.html.twig',["activateAccountMessageSuccess"=>$this->getLanguageStringValue("accountActivated")]);
                }
                else
                {
                    return $this->renderTemplate('registration/activateAccount.html.twig',['error' => null,"activateAccountMessageFailed"=>$this->getLanguageStringValue("accountActivatedFailed")]);
                }
            }
            else
            {
                return $this->renderTemplate('registration/activateAccount.html.twig',['error' => null,"activateAccountMessageFailed"=>$this->getLanguageStringValue("accountActivatedFailed")]);
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/register/checkIsEmailExist")
     */
    public function registerCheckEmail(Request $request)
    {
        try {
            $email = $request->get("Email");
            if($email != null)
            {
                if($this->getDoctrine()->getRepository(User::class)->count(["email"=>$email]) > 0)
                {
                    return Utilities::createSuccessfulResponse("exist");
                }
                else
                {
                    return Utilities::createErrorResponse("not exist");
                }
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

}