<?php


namespace App\Controller\User;
use App\Controller\Base\BaseUserController;
use App\Controller\Service\Stripe\StripeController;
use App\Controller\Service\Stripe\StripeCustomerController;
use App\Entity\Address;
use App\Repository\AddressRepository;
use App\util\Utilities;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UserProfileController extends BaseUserController
{
    /**
     * @Route("userprofile")
     * @IsGranted("ROLE_USER")
     */
    public function init(Request $request)
    {
        try {
            $breadcrumbPath = array();
            $breadcrumbPath[]  = array(
                "Url" => "/userprofile",
                "Name" => $this->getLanguageStringValue("userProfil")
            );
            return $this->renderTemplate('/user/userprofile.html.twig',[
                'SiteName' => $this->getLanguageStringValue("userProfil"),
                'NavName' => 'User',
                'BreadcrumbPath' => $breadcrumbPath,
                'FillAddresses' => $request->query->get("fillAddresses")
            ]);
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/userprofile/editGeneral")
     * @IsGranted("ROLE_USER")
     */
    public function editGeneral(Request $request,HttpClientInterface $httpClientInterface,LoggerInterface $logger)
    {

        try {
            $salutation = $request->request->get('Salutation');
            $firstname = $request->request->get('Firstname');
            $secondname = $request->request->get('Secondname');

            $street = $request->request->get('Street');
            $houseNumber = $request->request->get('HouseNumber');
            $place = $request->request->get('Place');
            $postCode = $request->request->get('PostCode');

            $billStreet = $request->request->get('BillStreet');
            $billHouseNumber = $request->request->get('BillHouseNumber');
            $billPlace = $request->request->get('BillPlace');
            $billPostCode = $request->request->get('BillPostCode');

            $billFirstname = $request->request->get('BillFirstname');
            $billSecondName = $request->request->get('BillSecondName');


            if($firstname == null || $secondname == null)
            {
                return Utilities::createErrorResponse($this->getLanguageStringValue("enterFirstAndSecondName"));
            }
            else
            {
                $stripeCustomerController = new StripeCustomerController($httpClientInterface,$logger);

                $this->getUser()
                    ->setFirstname($firstname)
                    ->setsecondname($secondname)
                    ->setSalutation($salutation);

                if($this->getUser()->getDeliveryAddress() != null)
                {
                    $this->getUser()->getDeliveryAddress()
                        ->setPostCode($postCode)
                        ->setStreet($street)
                        ->setStreetNumber($houseNumber)
                        ->setPlace($place)
                        ->setDifferentFirstname($billFirstname)
                        ->setDifferentSecondName($billSecondName)
                        ->setAddressType(Address::$ADDRESS_TYP_DELIVERY);
                    $this->getDoctrine()->getManager()->flush();
                }
                else
                {
                    $deliveryAddress = new Address();
                    $deliveryAddress->setPostCode($postCode)
                        ->setStreet($street)
                        ->setStreetNumber($houseNumber)
                        ->setPlace($place)
                        ->setDifferentFirstname($billFirstname)
                        ->setDifferentSecondName($billSecondName)
                        ->setAddressType(Address::$ADDRESS_TYP_DELIVERY);
                    $this->getDoctrine()->getManager()->persist($deliveryAddress);
                    $this->getUser()->setDeliveryAddress($deliveryAddress);
                    $this->getDoctrine()->getManager()->flush();
                }

                if($this->getUser()->getBillingAddress() != null)
                {
                    $this->getUser()->getBillingAddress()
                        ->setPostCode($billPostCode)
                        ->setStreet($billStreet)
                        ->setStreetNumber($billHouseNumber)
                        ->setPlace($billPlace)
                        ->setAddressType(Address::$ADDRESS_TYP_BILLING);
                    $this->getDoctrine()->getManager()->flush();
                }
                else
                {
                    $billingAddress = new Address();
                    $billingAddress->setPostCode($billPostCode)
                        ->setStreet($billStreet)
                        ->setStreetNumber($billHouseNumber)
                        ->setPlace($billPlace)
                        ->setAddressType(Address::$ADDRESS_TYP_BILLING);
                    $this->getDoctrine()->getManager()->persist($billingAddress);
                    $this->getUser()->setBillingAddress($billingAddress);
                    $this->getDoctrine()->getManager()->flush();

                }

                $stripeCustomerController->setName($this->getUser()->getStripeCustomerId(),$billFirstname." ".$billSecondName);
                $stripeCustomerController->setBillingAddress($this->getUser()->getStripeCustomerId(),$billStreet." ". $billHouseNumber,$billPlace,$billPostCode);
                $stripeCustomerController->setShippingAddress($this->getUser()->getStripeCustomerId(),$firstname." ".$secondname,$street." ". $houseNumber,$place,$postCode);
                return Utilities::createSuccessfulResponse($this->getLanguageStringValue("personaldataSaved"));
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/userprofile/editAdministration")
     * @IsGranted("ROLE_USER")
     */
    public function editAdministration(Request $request,UserPasswordEncoderInterface $passwordEncoder,HttpClientInterface $httpClientInterface,LoggerInterface $logger)
    {
        try {
            $passwordPlain = $request->request->get('Password');
            $validPassword = $passwordEncoder->isPasswordValid($this->getUser(), $passwordPlain);

            if($validPassword)
            {
                $newPassword = $request->request->get('NewPassword');
                $newPasswordRepeat = $request->request->get('NewPasswordRepeat');

                if($newPassword != null)
                {
                    if($newPassword == $newPasswordRepeat)
                    {
                        $password = $passwordEncoder->encodePassword($this->getUser(), $newPassword);
                        $this->getUser()->setPassword($password);

                    }
                    else
                    {
                        return Utilities::createErrorResponse($this->getLanguageStringValue("newPasswortNotMatch"));
                    }
                }

                $newEmail = $request->request->get('NewEmail');
                $newEmailRepeat = $request->request->get('NewEmailRepeat');
                if($newEmail != null)
                {
                    if($newEmail == $newEmailRepeat)
                    {
                        $this->getUser()->setEmail($newEmail);
                        $stripeCustomerController = new StripeCustomerController($httpClientInterface,$logger);
                        $stripeCustomerController->setEMail($this->getUser()->getStripeCustomerId(),$newEmail);
                    }
                    else
                    {
                        return Utilities::createErrorResponse($this->getLanguageStringValue("newEmailNotMatch"));
                    }
                }
                $this->getDoctrine()->getManager()->flush();
                return Utilities::createSuccessfulResponse($this->getLanguageStringValue("accontDataSaved"));

            }
            else
            {
                return Utilities::createErrorResponse($this->getLanguageStringValue("oldPasswordNotRight"));
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/userprofile/editProfilePicture")
     * @IsGranted("ROLE_USER")
     */
    public function editProfilePicture(Request $request)
    {
        try {
            $file = $request->files->get('fileToUpload');
            if ($file != null)
            {
                $filePath = $file->getPathname();
                $type = $_FILES["fileToUpload"]["type"];
                $data = file_get_contents($filePath);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $this->getUser()->setProfilePicture($base64);
                $this->getDoctrine()->getManager()->flush();

                return Utilities::createSuccessfulResponse($this->getLanguageStringValue("profilePictureSaved"));
            }
            else
            {
                return Utilities::createErrorResponse($this->getLanguageStringValue("errorContactAdmin"));
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/userprofile/removeProfilePicture")
     * @IsGranted("ROLE_USER")
     */
    public function removeProfilePicture()
    {
        try {
            $this->getUser()->setProfilePicture(null);
            $this->getDoctrine()->getManager()->flush();
            return Utilities::createSuccessfulResponse($this->getLanguageStringValue("profilePictureSaved"));
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

}