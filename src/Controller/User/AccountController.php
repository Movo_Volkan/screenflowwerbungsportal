<?php


namespace App\Controller\User;
use App\Controller\Base\BaseController;
use App\Controller\Base\EMailController;
use App\Entity\User;
use App\util\AppConfigs;
use App\util\Utilities;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends BaseController
{
    /**
     * @Route("/resetPasswordRequest")
     */
    public function init()
    {
        try {
            return $this->renderTemplate('resetpassword/resetpasswordrequest.html.twig',[
                'SiteName' => $this->getLanguageStringValue("resetPassword"),
                'NavName' => 'ResetPasswordRequest'
            ]);
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("/resetPasswordRequest/request")
     */
    public function resetPasswordRequest(Request $request,MailerInterface $mailer,LoggerInterface $logger)
    {
        try {
            $eMail = $request->get('EMail');

            if($eMail != null)
            {
                $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email"=>$eMail]);
                if($user != null && ($this->getAppStage() == AppConfigs::$APP_STAGE_PROD || $this->getAppStage() == AppConfigs::$APP_STAGE_TEST))
                {
                    $user->setPasswordResetCode(Utilities::UuidV1());
                    $this->getDoctrine()->getManager()->flush();
                    $emailController = new EMailController($mailer,$logger);
                    $emailController->createTemplatedEmail('service@movods.com',
                        $this->getLanguageStringValue("resetPasswordSubject"),
                        $eMail,
                        $this->getLanguageStringValue("resetPasswordSubject"),
                        'emails/resetpassword.html.twig',
                        [
                            'LanguageStrings' =>Utilities::buildLanguageStrings(),
                            'PasswordResetCode' => $user->getPasswordResetCode(),
                            'CurrentYear' => date("Y")
                        ]);

                    $emailController->sendEmail();
                }
                return Utilities::createSuccessfulResponse($this->getLanguageStringValue("searchEmailToResetPassword"));
            }
            else
            {
                return Utilities::createErrorResponse($this->getLanguageStringValue("errorContactAdmin"));
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("resetPassword")
     */
    public function resetPassword(Request $request)
    {
        try {
            $prc = $request->query->get('PRC');

            if($prc != null)
            {
                $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["passwordResetCode"=>$prc]);
                if ($user != null)
                {
                    return $this->renderTemplate("resetpassword/resetpassword.html.twig",
                        [
                            "SiteName"=>$this->getLanguageStringValue("resetPassword"),
                            "PasswordResetCode"=>$prc
                        ]
                    );
                }
                else
                {
                    return $this->render404();
                }
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }

    /**
     * @Route("resetPassword/reset")
     */
    public function resetPasswordReset(Request $request,UserPasswordEncoderInterface $passwordEncoder)
    {
        try {
            $prc = $request->get('PRC');
            $newPassword = $request->get('NewPassword');
            $newPasswordRepeat = $request->get('NewPasswordRepeat');
            if($prc != null && $newPassword != null && $newPasswordRepeat != null)
            {
                if($newPassword == $newPasswordRepeat)
                {
                    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["passwordResetCode"=>$prc]);
                    if($user != null)
                    {
                        $newHashedPassword = $passwordEncoder->encodePassword($user,$newPassword);
                        $user->setPassword($newHashedPassword);
                        $user->setPasswordResetCode(null);
                        $this->getDoctrine()->getManager()->flush();
                        return Utilities::createSuccessfulResponse($this->getLanguageStringValue("passwordChanged"));
                    }
                    else
                    {
                        return Utilities::createErrorResponse($this->getLanguageStringValue("errorContactAdmin"));
                    }
                }
                else
                {
                    return Utilities::createErrorResponse($this->getLanguageStringValue("passwordMatchFailed"));
                }
            }
            else
            {
                return Utilities::createErrorResponse($this->getLanguageStringValue("errorContactAdmin"));
            }
        }
        catch (Exception $exception)
        {
            $this->getLogger()->error($exception);
        }
    }
}