<?php

namespace App\Controller;

use App\Entity\Media;
use App\util\Utilities;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdvertiseController extends AbstractController
{
    /**
     * @Route("/api/advertise", methods={"GET"})
     */
    public function advertise(Request $request): ?Response
    {
        $realTimeData = $request->get('real_time_data');
        $category = $request->get('category');

        $advertiseList = $this->getDoctrine()->getRepository(Media::class)->findAdvertisesByCategory($category);

        if (!empty($advertiseList))
        {
            $realTimeDataAsJson = json_decode($realTimeData, true);

            $genderAgeData = $realTimeDataAsJson[0]["genderAgeData"];
            $objects = $realTimeDataAsJson[1]["objects"];

            $advertisesWithAgeGender = array();

            foreach($genderAgeData as $genderAgeItem)
            {
                $age = $genderAgeItem['age'];
                $gender = $genderAgeItem['gender'];

                foreach ($advertiseList as $advertiseItem)
                {
                    if (($advertiseItem->getAgeCategory() == $age && $advertiseItem->getGender() == $gender)
                        || ($advertiseItem->getAgeCategory() == "All" && $advertiseItem->getGender() == $gender)
                        || ($advertiseItem->getGender() == "All" && $advertiseItem->getAgeCategory() == $age)
                        || ($advertiseItem->getAgeCategory() == "All" && $advertiseItem->getGender() == "All"))
                    {
                        if (!empty($advertisesWithAgeGender))
                        {
                            if (!in_array($advertiseItem, $advertisesWithAgeGender))
                            {
                                array_push($advertisesWithAgeGender, $advertiseItem);
                            }
                        }
                        else
                        {
                            array_push($advertisesWithAgeGender, $advertiseItem);
                        }
                    }
                }
            }

            $possibleAdvertisesList = array();

            foreach ($objects as $object)
            {
                foreach ($advertisesWithAgeGender as $advertiseItem)
                {
                    if (str_contains($advertiseItem->getTags(), key($object)))
                    {
                        if (!empty($possibleAdvertisesList))
                        {
                            if (!in_array($advertiseItem, $possibleAdvertisesList))
                            {
                                array_push($possibleAdvertisesList, $advertiseItem);
                            }
                        }
                        else
                        {
                            array_push($possibleAdvertisesList, $advertiseItem);
                        }
                    }
                }
            }

            if (!empty($possibleAdvertisesList))
            {
                $advertisingMedia = $possibleAdvertisesList[0];
                $file = new File($advertisingMedia->getPath());


                $response = $this->file($file, "advertise." . $file->getExtension());

                $response->headers->set("ScreenID", $advertisingMedia->getId());

                return $response;
            }
        }
        return Utilities::createErrorResponse("Advertisement not found", 404);
    }
}
