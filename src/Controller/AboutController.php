<?php


namespace App\Controller;
use App\Controller\Base\BaseUserController;
use App\Entity\Version;
use App\util\Utilities;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends BaseUserController
{
    /**
     * @Route("/about")
     * @IsGranted("ROLE_USER")
     */
    public function init()
    {
        $breadcrumbPath = array();
        $breadcrumbPath[]  = array(
            "Url" => "/about",
            "Name" => $this->getLanguageStringValue("about")
        );
        return $this->renderTemplate('/about.html.twig',[
            'SiteName' => $this->getLanguageStringValue("about"),
            'NavName' => 'About',
            'BreadcrumbPath' => $breadcrumbPath,
            'Versions' => $this->getDoctrine()->getRepository(Version::class)->findAll()
        ]);
    }
}