let widgets = [];

class Widget {
    constructor(id, positionX, positionY, height, width, zIndex) {

        if(id == null || id=== undefined || id === "") {
            this.id = this.uuidv4();
        }
        else {
            this.id = id;
        }
        
        this.positionX = positionX;
        this.positionY = positionY;

        if(isNaN(this.positionX) || isNaN(this.positionY)) 
        {
            this.positionX = 0;
            this.positionY = 0;
        }

        this.originalPositionX = positionX;
        this.originalPositionY = positionY;

        this.height = height;
        this.width = width;

        this.originalHeight = height;
        this.originalWidth = width;

        this.typeName = "Widget";
        this.htmlElement = document.createElement("div");
        this.htmlElement.id = this.id;
        this.htmlElement.style.left = this.positionX + "px";
        this.htmlElement.style.top = this.positionY + "px";
        this.htmlElement.style.height = this.height + "px";
        this.htmlElement.style.width = this.width + "px";
        this.htmlElement.style.position = "absolute";
        if(zIndex > -1) {
            this.zIndex = zIndex;
            this.htmlElement.style.zIndex = this.zIndex;
        }
        else {
            this.zIndex = getWidgets().length + 1;
            this.htmlElement.style.zIndex = getWidgets().length + 1;
        }
        this.htmlElement.tabIndex = 0;
        this.htmlElement.widget = this;
        this.htmlElement.owner = this.getHtmlElement();
        console.log("Widget with ID: " + this.id + " created.");
    }

    setPositionX(positionX) {
        this.positionX = positionX;
        this.htmlElement.style.left = this.positionX + "px";
    }

    getPositionX() {
        return this.positionX;
    }

    setPositionY(positionY) {
        this.positionY = positionY;
        this.htmlElement.style.top = this.positionY + "px";
    }

    getPositionY() {
        return this.positionY;
    }

    setHeight(height) {
        this.height = height;
        this.htmlElement.style.height = this.height + "px";
    }

    getHeight() {
        return this.height;
    }

    setWidth(width) {
        this.width = width;
        this.htmlElement.style.width = this.width + "px";
    }

    getWidth() {
        return this.width;
    }

    setZIndex(zIndex) {
        this.zIndex = zIndex;
        this.htmlElement.style.zIndex  = this.zIndex;
    }

    getZIndex() {
        return this.zIndex;
    }

    getHtmlElement() {
        return this.htmlElement;
    }

    getOriginalHeight() {
        return this.originalHeight;
    }

    getOriginalWidth() {
        return this.originalWidth;
    }

    getOriginalPositionX() {
        return this.originalPositionX;
    }

    getOriginalPositionY() {
        return this.originalPositionY;
    }

    selectElement() {       
        let layerPanel = document.getElementById("layerPanel");
        for (let i = 0; i < layerPanel.children.length; i++) {
            layerPanel.children[i].classList.remove("ui-layer-state-default-selected");
            layerPanel.children[i].classList.remove("selected-element");
            if(layerPanel.children[i].owner.id === this.id) {
                this.htmlElement.classList.add("selected-element");
                layerPanel.children[i].classList.add("ui-layer-state-default-selected");
                selectedElement = this;
            }
        }

        for (let child of this.htmlElement.children) {
            
            for (let cssClass of child.classList) {
                
                if(cssClass == "ui-resizable-handle")
                {
                    child.style = "";
                }
            }
        }
    }

    deselectElement() 
    {
        this.htmlElement.classList.remove("selected-element");
        for (let child of this.htmlElement.children) {
            
            for (let cssClass of child.classList) {
                
                if(cssClass == "ui-resizable-handle")
                {
                    child.style = "display:none;"
                }
            }
        }
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }
}

class ImageWidget extends Widget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
        super(id, positionX, positionY, height, width, zIndex);
        this.sourceUrl = sourceUrl;
        this.thumbnail = thumbnail;
        this.typeName = "ImageWidget";
        this.imageHtmlElement = document.createElement("img");
        this.imageHtmlElement.style.height = "100%";
        this.imageHtmlElement.style.width = "100%";
        this.imageHtmlElement.src = this.sourceUrl;
        this.imageHtmlElement.owner = this.getHtmlElement();
        this.metaData = metaData;
        this.getHtmlElement().appendChild(this.imageHtmlElement);
    }

    setSourceUrl(sourceUrl) {
        this.sourceUrl = sourceUrl;
        this.imageHtmlElement.src = this.sourceUrl;
    }

    getSourceUrl() {
        return this.sourceUrl;
    }

    setThumbnail(thumbnail){
        this.thumbnail = thumbnail;
    }

    getThumbnail(){
        return this.thumbnail;
    }

    setTypeName(typeName) {
        this.typeName = typeName;
    }
}

class VideoWidget extends Widget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail,metaData){
        super(id, positionX,positionY,height, width, zIndex);
        this.sourceUrl = sourceUrl;
        this.thumbnail = thumbnail;
        this.typeName = "VideoWidget";
        this.videoHtmlElement = document.createElement("video");
        this.videoHtmlElement.style.height = "100%";
        this.videoHtmlElement.style.width = "100%";
        this.videoHtmlElement.style.objectFit = "unset";
        this.videoHtmlElement.src = this.sourceUrl + "#t=0.1";
        this.videoHtmlElement.owner = this.getHtmlElement();
        this.metaData = metaData;
        this.getHtmlElement().appendChild(this.videoHtmlElement);
    }

    setSourceUrl(sourceUrl) {
        this.sourceUrl = sourceUrl;
        this.videoHtmlElement.src = this.sourceUrl;
    }

    getSourceUrl() {
        return this.sourceUrl;
    }

    setThumbnail(thumbnail){
        this.thumbnail = thumbnail;
    }

    getThumbnail(){
        return this.thumbnail;
    }

    setTypeName(typeName) {
        this.typeName = typeName;
    }
}

class ExternImageWidget extends ImageWidget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
        super(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail,metaData);
        this.typeName = "ExternImageWidget";
    }
}

class ExternVideoWidget extends VideoWidget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
        super(id, positionX,positionY,height, width, zIndex, sourceUrl, thumbnail, metaData);
        this.typeName = "ExternVideoWidget";
    }
}

class TextWidget extends Widget {
    constructor(id, positionX,positionY,height, width, zIndex, text, fontSize, fontWeight, color, backgroundColor){
        super(id, positionX,positionY,height, width, zIndex);
        this.text = text;
        this.fontSize = fontSize;
        this.fontWeight = fontWeight;
        this.color = color;
        this.backgroundColor = backgroundColor;
        this.typeName = "TextWidget";
        this.textHtmlElement = document.createElement("div");
        this.textHtmlElement.innerHTML = this.text;
        this.textHtmlElement.style.height = "100%";
        this.textHtmlElement.style.width = "100%";
        this.textHtmlElement.style.fontSize = this.fontSize + "px";
        this.textHtmlElement.style.fontWeight = this.fontWeight;
        this.textHtmlElement.style.color = this.color;
        this.textHtmlElement.style.backgroundColor = this.backgroundColor;
        this.textHtmlElement.style.overflowWrap = "break-word";
        this.textHtmlElement.style.overflow = "hidden";
        this.textHtmlElement.contentEditable = "true";
        this.textHtmlElement.setAttribute("contenteditable", "true");
        this.textHtmlElement.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.textHtmlElement);
        this.textHtmlElement.addEventListener("input", function(element) {
            element.target.parentElement.widget.setTextProperty(element.target.textContent);
        }, false);
    }

    setTextProperty(text){
        this.text = text;
    }

    setText(text) {
        this.text = text;
        this.textHtmlElement.innerHTML = this.text;
    }

    getText() {
        return this.text;
    }

    setFontSize(fontSize) {
        this.fontSize = fontSize;
        this.textHtmlElement.style.fontSize = this.fontSize + "px";
    }

    getFontSize() {
        return this.fontSize;
    }

    setFontWeight(fontWeight) {
        this.fontWeight = fontWeight;
        this.textHtmlElement.style.fontWeight = this.fontWeight;
    }

    getFontWeight() {
        return this.fontWeight;
    }

    setColor(color) {
        this.color = color;
        this.textHtmlElement.style.color = this.color;
    }

    getColor() {
        return this.color;
    }

    setBackgroundColor(backgroundColor) {
        this.backgroundColor = backgroundColor;
        this.textHtmlElement.style.backgroundColor = this.backgroundColor;
    }

    getBackgroundColor(){
        return this.backgroundColor;
    }
}

class TickerWidget extends TextWidget {
    constructor(id, positionX,positionY,height, width, zIndex, text, fontSize, fontWeight, color, backgroundColor){
        super(id, positionX,positionY,height, width, zIndex, text, fontSize, fontWeight, color, backgroundColor);
        this.duration = 25;
        this.typeName = "TickerWidget";

        this.getHtmlElement().firstChild.innerHTML = "";
        this.tickerTextHTML = document.createElement("div");
        this.tickerTextHTML.style.position = "relative";
        this.tickerTextHTML.owner = this.getHtmlElement();

        this.tickerText = document.createElement("p");
        this.tickerText.style.position = "absolute";
        this.tickerText.innerHTML = this.text;
        this.tickerText.style.left = this.width + "px";
        this.tickerText.owner = this.getHtmlElement();
        this.tickerTextHTML.appendChild(this.tickerText);
        this.getHtmlElement().firstChild.appendChild(this.tickerTextHTML);
        this.tickerInterval = this.setupSlideEffect();
        
    }

    setupSlideEffect() {
       return setInterval(function(widget) { 
            let left = parseInt(widget.tickerText.style.left, 10);
            if(left <= widget.width) {
                widget.tickerText.style.left = left - 1 + "px";
                if(left < -widget.tickerText.clientWidth){
                    widget.tickerText.style.left = widget.width + "px";
                }
            }

        }, this.getDuration(), this);
    }

    getDuration() {
        return this.duration;
    }

    setDuration(duration) {
        this.duration = duration;
        clearInterval(this.tickerInterval);
        this.tickerInterval = this.setupSlideEffect();
    }

    clearSlideEffect() {
        clearInterval(this.tickerInterval);
    }

    setText(text) {
        this.text = text;
        this.tickerText.innerHTML = this.text;
    }
}

class QRCodeWidget extends Widget {
    constructor(id, positionX,positionY,height, width, zIndex, text) {
        super(id, positionX,positionY,height, width, zIndex);

        this.text = text;
        this.typeName = "QRCodeWidget";
        this.qrCodeDiv = document.createElement("div");
 
        var qrcode = new QRCode(this.qrCodeDiv, {
            text: this.text,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H
        });
        this.qrCodeImage = qrcode._oDrawing._elImage;
        this.qrCodeImage.style.height = "100%";
        this.qrCodeImage.style.width = "100%";
        this.qrCodeImage.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.qrCodeImage);
    }
}

class HTMLWidget extends Widget {
    constructor(id, positionX,positionY,height, width, zIndex, htmlCode) {
        super(id, positionX,positionY,height, width, zIndex);

        this.typeName = "HTMLWidget";
        this.htmlCode = htmlCode;
        this.htmlCodeHtmlElement = document.createElement("div");
        this.htmlCodeHtmlElement.innerHTML = this.htmlCode;
        this.htmlCodeHtmlElement.style.height = "100%";
        this.htmlCodeHtmlElement.style.width = "100%";
        this.htmlCodeHtmlElement.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.htmlCodeHtmlElement);
    }

    setHtmlCode(htmlCode) {
        this.htmlCode = htmlCode;
        this.htmlCodeHtmlElement.innerHTML = this.htmlCode;
        for (let i = 0; i < this.htmlCodeHtmlElement.children.length; i++) {
            this.htmlCodeHtmlElement.children[i].owner = this.getHtmlElement();
        }
    }

    getHTMLCode() {
        return this.htmlCode;
    }
}

$(document).ready(function () {
    onInit();
});

function onInit() {
    widgetMapper("{\r\n    \"widgets\":[\r\n        {\r\n            \"id\":\"9c0f0a2c-55b4-464f-a529-7f96d79c0041\",\r\n            \"positionX\":0,\r\n            \"positionY\":0,\r\n            \"height\":490,\r\n            \"width\":450,\r\n            \"typeName\":\"ImageWidget\",\r\n            \"htmlElement\":\"\",\r\n            \"zIndex\":1,\r\n            \"sourceUrl\":\"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n            \"thumbnail\":\"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n        },\r\n        {\r\n            \"id\":\"9c0f0a2c-55b4-464f-a529-7f96d79c0021\",\r\n            \"positionX\":300,\r\n            \"positionY\":200,\r\n            \"height\":50,\r\n            \"width\":50,\r\n            \"typeName\":\"ImageWidget\",\r\n            \"htmlElement\":\"\",\r\n            \"zIndex\":1,\r\n            \"sourceUrl\":\"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n            \"thumbnail\":\"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n        }\r\n    ]\r\n}");
    resizeElements(490, 900);
}

function widgetMapper(jsonString) {
    const jsonObj = JSON.parse(jsonString);
    jsonObj.widgets.forEach((widget => {
        if(widget.typeName === "ImageWidget") {
            addWidget(new ImageWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail,{"mediaId":widget.mediaId}));
        }
        else if(widget.typeName === "VideoWidget") {
            addWidget(new VideoWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail),{"mediaId":widget.mediaId});
        }
        else if(widget.typeName === "TextWidget") {
            addWidget(new TextWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.text , widget.fontSize,widget.fontWeight,widget.color,widget.backgroundColor));
        }
        else if(widget.typeName === "TickerWidget") {
            addWidget(new TickerWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.text , widget.fontSize,widget.fontWeight,widget.color,widget.backgroundColor));
        }
        else if(widget.typeName === "HTMLWidget") {
            addWidget(new HTMLWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex,widget.code));
        }
        else if(widget.typeName === "ExternImageWidget") {
            addWidget(new ExternImageWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail,{}));
        }
        else if(widget.typeName === "ExternVideoWidget") {
            addWidget(new ExternVideoWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail),{});
        }
        else if(widget.typeName === "QrCodeWidget") {
            addWidget(new QRCodeWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.text));
        }
    }));
}

function addWidget(widget) {
    getWidgets().push(widget);
}

function getWidgets() {
    return widgets;
}

function resizeElements(editorHeight, editorWidth) {
    let preview = document.getElementById("preview");
    preview.innerHTML = "";
    getWidgets().forEach(widget => {
        let element = widget.getHtmlElement();
        preview.appendChild(element);
        resizeElement(element, editorHeight, editorWidth);
    });
}

function resizeElement(element, editorHeight, editorWidth) {

    const resolutionWidth = viewWidth();
    const resolutionHeight = viewHeight();

    const elementHeight = element.widget.getOriginalHeight();
    const elementWidth = element.widget.getOriginalWidth();
    const pHeight = elementHeight / editorHeight;
    const pWidth = elementWidth / editorWidth;

    const outputHeight = resolutionHeight * pHeight;
    element.widget.setHeight(outputHeight);

    const outputWidth = resolutionWidth * pWidth;
    element.style.width = outputWidth + "px";

    const elementXPositionInContainer = element.widget.getOriginalPositionX();
    const elementYPositionInContainer = element.widget.getOriginalPositionY();

    const pxPos = elementXPositionInContainer / editorWidth;
    const pyPos = elementYPositionInContainer / editorHeight;

    const outputXPos = resolutionWidth * pxPos;
    element.widget.setPositionX(outputXPos);

    const outputYPos = resolutionHeight * pyPos;
    element.widget.setPositionY(outputYPos);

}

function viewWidth() {
    return window.innerWidth 
        || document.documentElement.clientWidth 
        || document.body.clientWidth 
        || 0;
 }
 
 function viewHeight() {
    return window.innerHeight 
        || document.documentElement.clientHeight 
        || document.body.clientHeight 
        || 0;
 }