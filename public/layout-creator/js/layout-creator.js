let widgets = [];
let inserts = [];

let selectedElement;
var zoomScale = 1;

const defaultWidgetHeight = 150;
const defaultWidgetWidth = 150;

class Widget {
    constructor(id, positionX, positionY, height, width, zIndex) {

        if(id == null || id=== undefined || id === "") {
            this.id = this.uuidv4();
        }
        else {
            this.id = id;
        }
        
        this.positionX = positionX;
        this.positionY = positionY;

        if(isNaN(this.positionX) || isNaN(this.positionY)) 
        {
            this.positionX = 0;
            this.positionY = 0;
        }

        this.height = height;
        this.width = width;
        this.typeName = "Widget";
        this.htmlElement = document.createElement("div");
        this.htmlElement.id = this.id;
        this.htmlElement.style.left = this.positionX + "px";
        this.htmlElement.style.top = this.positionY + "px";
        this.htmlElement.style.height = this.height + "px";
        this.htmlElement.style.width = this.width + "px";
        this.htmlElement.style.position = "absolute";
        if(zIndex > -1) {
            this.zIndex = zIndex;
            this.htmlElement.style.zIndex = this.zIndex;
        }
        else {
            this.zIndex = getWidgets().length + 1;
            this.htmlElement.style.zIndex = getWidgets().length + 1;
        }
        this.htmlElement.tabIndex = 0;
        this.htmlElement.widget = this;
        this.htmlElement.owner = this.getHtmlElement();
        this.htmlElement.addEventListener("mousedown", function(element) {

            let elementOwner;
            //Try to find parent with owner for TextWidget case
            if(element.target.owner === undefined)
            {
                let elementTarget = element.target
                while (elementTarget.parentNode)
                {
                    if(elementTarget.owner !== undefined && elementTarget.owner.widget.constructor.name === "TextWidget")
                    {
                        elementOwner = elementTarget.owner;
                        break;
                    }

                    elementTarget = elementTarget.parentNode;
                }
            }

            if(element.target.owner !== undefined) {
                deselectAll();
                refreshPropertiesPanel(element.target.owner);
                element.target.owner.widget.selectElement();
            }
            else {
                deselectAll();
                refreshPropertiesPanel(elementOwner);
                elementOwner.widget.selectElement();
            }
        });
        console.log("Widget with ID: " + this.id + " created.");
    }

    setPositionX(positionX) {
        this.positionX = positionX;
        this.htmlElement.style.left = this.positionX + "px";
    }

    getPositionX() {
        return this.positionX;
    }

    setPositionY(positionY) {
        this.positionY = positionY;
        this.htmlElement.style.top = this.positionY + "px";
    }

    getPositionY() {
        return this.positionY;
    }

    setHeight(height) {
        this.height = height;
        this.htmlElement.style.height = this.height + "px";
    }

    getHeight() {
        return this.height;
    }

    setWidth(width) {
        this.width = width;
        this.htmlElement.style.width = this.width + "px";
    }

    getWidth() {
        return this.width;
    }

    setZIndex(zIndex) {
        this.zIndex = zIndex;
        this.htmlElement.style.zIndex  = this.zIndex;
    }

    getZIndex() {
        return this.zIndex;
    }

    getHtmlElement() {
        return this.htmlElement;
    }

    selectElement() {       
        let layerPanel = document.getElementById("layerPanel");
        for (let i = 0; i < layerPanel.children.length; i++) {
            layerPanel.children[i].classList.remove("ui-layer-state-default-selected");
            layerPanel.children[i].classList.remove("selected-element");
            if(layerPanel.children[i].owner.id === this.id) {
                this.htmlElement.classList.add("selected-element");
                layerPanel.children[i].classList.add("ui-layer-state-default-selected");
                selectedElement = this;
            }
        }

        for (let child of this.htmlElement.children) {
            
            for (let cssClass of child.classList) {
                
                if(cssClass == "ui-resizable-handle")
                {
                    child.style = "";
                }
            }
        }
    }

    deselectElement() 
    {
        this.htmlElement.classList.remove("selected-element");
        for (let child of this.htmlElement.children) {
            
            for (let cssClass of child.classList) {
                
                if(cssClass == "ui-resizable-handle")
                {
                    child.style = "display:none;"
                }
            }
        }
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }
}

class ImageWidget extends Widget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
        super(id, positionX, positionY, height, width, zIndex);
        this.sourceUrl = sourceUrl;
        this.thumbnail = thumbnail;
        this.typeName = "ImageWidget";
        this.imageHtmlElement = document.createElement("img");
        this.imageHtmlElement.style.height = "100%";
        this.imageHtmlElement.style.width = "100%";
        this.imageHtmlElement.src = this.sourceUrl;
        this.imageHtmlElement.owner = this.getHtmlElement();
        this.metaData = metaData;
        this.getHtmlElement().appendChild(this.imageHtmlElement);
    }

    setSourceUrl(sourceUrl) {
        this.sourceUrl = sourceUrl;
        this.imageHtmlElement.src = this.sourceUrl;
    }

    getSourceUrl() {
        return this.sourceUrl;
    }

    setThumbnail(thumbnail){
        this.thumbnail = thumbnail;
    }

    getThumbnail(){
        return this.thumbnail;
    }

    setTypeName(typeName) {
        this.typeName = typeName;
    }
}

class VideoWidget extends Widget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail,metaData){
        super(id, positionX,positionY,height, width, zIndex);
        this.sourceUrl = sourceUrl;
        this.thumbnail = thumbnail;
        this.typeName = "VideoWidget";
        this.videoHtmlElement = document.createElement("video");
        this.videoHtmlElement.style.height = "100%";
        this.videoHtmlElement.style.width = "100%";
        this.videoHtmlElement.style.objectFit = "unset";
        this.videoHtmlElement.src = this.sourceUrl + "#t=0.1";
        this.videoHtmlElement.owner = this.getHtmlElement();
        this.metaData = metaData;
        this.getHtmlElement().appendChild(this.videoHtmlElement);
    }

    setSourceUrl(sourceUrl) {
        this.sourceUrl = sourceUrl;
        this.videoHtmlElement.src = this.sourceUrl;
    }

    getSourceUrl() {
        return this.sourceUrl;
    }

    setThumbnail(thumbnail){
        this.thumbnail = thumbnail;
    }

    getThumbnail(){
        return this.thumbnail;
    }

    setTypeName(typeName) {
        this.typeName = typeName;
    }
}


class ExternImageWidget extends ImageWidget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
        super(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail,metaData);
        this.typeName = "ExternImageWidget";
    }
}

class ExternVideoWidget extends VideoWidget {
    constructor(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
        super(id, positionX,positionY,height, width, zIndex, sourceUrl, thumbnail, metaData);
        this.typeName = "ExternVideoWidget";
    }
}

class TextWidget extends Widget {
    constructor(id, positionX,positionY,height, width, zIndex, text, fontSize, fontWeight, color, backgroundColor){
        super(id, positionX,positionY,height, width, zIndex);
        this.text = text;
        this.fontSize = fontSize;
        this.fontWeight = fontWeight;
        this.color = color;
        this.backgroundColor = backgroundColor;
        this.typeName = "TextWidget";
        this.textHtmlElement = document.createElement("div");
        this.textHtmlElement.innerHTML = this.text;
        this.textHtmlElement.style.height = "100%";
        this.textHtmlElement.style.width = "100%";
        this.textHtmlElement.style.fontSize = this.fontSize + "px";
        this.textHtmlElement.style.fontWeight = this.fontWeight;
        this.textHtmlElement.style.color = this.color;
        this.textHtmlElement.style.backgroundColor = this.backgroundColor;
        this.textHtmlElement.style.overflowWrap = "break-word";
        this.textHtmlElement.style.overflow = "hidden";
        this.textHtmlElement.contentEditable = "true";
        this.textHtmlElement.setAttribute("contenteditable", "true");
        this.textHtmlElement.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.textHtmlElement);

        this.textHtmlElement.addEventListener("input", function(element) {
            console.log("input on event")
            element.target.parentElement.widget.setTextProperty(element.target.textContent);
        }, false);

        this.textHtmlElement.addEventListener("focus", function(element) {
            $(this.parentElement).draggable("option", "disabled", true);
            this.parentElement.classList.add("selected-element-contenteditable");
            this.parentElement.classList.remove("selected-element")
        }, false);

        this.textHtmlElement.addEventListener("focusout", function(element) {
            $(this.parentElement).draggable("option", "disabled", false);
            this.parentElement.classList.remove("selected-element-contenteditable");
        }, false);

        this.textHtmlElement.addEventListener("dblclick", function(element) {
            $(this.parentElement).draggable("option", "disabled", true);
            this.parentElement.classList.add("selected-element-contenteditable");
            this.parentElement.classList.remove("selected-element")
            $(this).focus();
        }, false);

        this.textHtmlElement.addEventListener("click", function(element) {
           console.log("klick");
        }, false);

    }

    setTextProperty(text){
        this.text = text;
    }

    setText(text) {
        this.text = text;
        this.textHtmlElement.innerHTML = this.text;
    }

    getText() {
        return this.text;
    }

    setFontSize(fontSize) {
        this.fontSize = fontSize;
        this.textHtmlElement.style.fontSize = this.fontSize + "px";
    }

    getFontSize() {
        return this.fontSize;
    }

    setFontWeight(fontWeight) {
        this.fontWeight = fontWeight;
        this.textHtmlElement.style.fontWeight = this.fontWeight;
    }

    getFontWeight() {
        return this.fontWeight;
    }

    setColor(color) {
        this.color = color;
        this.textHtmlElement.style.color = this.color;
    }

    getColor() {
        return this.color;
    }

    setBackgroundColor(backgroundColor) {
        this.backgroundColor = backgroundColor;
        this.textHtmlElement.style.backgroundColor = this.backgroundColor;
    }

    getBackgroundColor(){
        return this.backgroundColor;
    }
}

class TickerWidget extends TextWidget {
    constructor(id, positionX,positionY,height, width, zIndex, text, fontSize, fontWeight, color, backgroundColor){
        super(id, positionX,positionY,height, width, zIndex, text, fontSize, fontWeight, color, backgroundColor);
        this.duration = 25;
        this.typeName = "TickerWidget";

        this.getHtmlElement().firstChild.innerHTML = "";
        this.tickerTextHTML = document.createElement("div");
        this.tickerTextHTML.style.position = "relative";
        this.tickerTextHTML.owner = this.getHtmlElement();

        this.tickerText = document.createElement("p");
        this.tickerText.style.position = "absolute";
        this.tickerText.innerHTML = this.text;
        this.tickerText.style.left = this.width + "px";
        this.tickerText.owner = this.getHtmlElement();
        this.tickerTextHTML.appendChild(this.tickerText);
        this.getHtmlElement().firstChild.appendChild(this.tickerTextHTML);
        this.tickerInterval = this.setupSlideEffect();
        
    }

    setupSlideEffect() {
       return setInterval(function(widget) { 
            let left = parseInt(widget.tickerText.style.left, 10);
            if(left <= widget.width) {
                widget.tickerText.style.left = left - 1 + "px";
                if(left < -widget.tickerText.clientWidth){
                    widget.tickerText.style.left = widget.width + "px";
                }
            }

        }, this.getDuration(), this);
    }

    getDuration() {
        return this.duration;
    }

    setDuration(duration) {
        this.duration = duration;
        clearInterval(this.tickerInterval);
        this.tickerInterval = this.setupSlideEffect();
    }

    clearSlideEffect() {
        clearInterval(this.tickerInterval);
    }

    setText(text) {
        this.text = text;
        this.tickerText.innerHTML = this.text;
    }
}

class QRCodeWidget extends Widget {
    constructor(id, positionX,positionY,height, width, zIndex, text) {
        super(id, positionX,positionY,height, width, zIndex);

        this.text = text;
        this.typeName = "QRCodeWidget";
        this.qrCodeDiv = document.createElement("div");
 
        var qrcode = new QRCode(this.qrCodeDiv, {
            text: this.text,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H
        });
        this.qrCodeImage = qrcode._oDrawing._elImage;
        this.qrCodeImage.style.height = "100%";
        this.qrCodeImage.style.width = "100%";
        this.qrCodeImage.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.qrCodeImage);
    }
}

class HTMLWidget extends Widget {
    constructor(id, positionX,positionY,height, width, zIndex, htmlCode) {
        super(id, positionX,positionY,height, width, zIndex);

        this.typeName = "HTMLWidget";
        this.htmlCode = htmlCode;
        this.htmlCodeHtmlElement = document.createElement("div");
        this.htmlCodeHtmlElement.innerHTML = this.htmlCode;
        this.htmlCodeHtmlElement.style.height = "100%";
        this.htmlCodeHtmlElement.style.width = "100%";
        this.htmlCodeHtmlElement.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.htmlCodeHtmlElement);
    }

    setHtmlCode(htmlCode) {
        this.htmlCode = htmlCode;
        this.htmlCodeHtmlElement.innerHTML = this.htmlCode;
        for (let i = 0; i < this.htmlCodeHtmlElement.children.length; i++) {
            this.htmlCodeHtmlElement.children[i].owner = this.getHtmlElement();
        }
    }

    getHTMLCode() {
        return this.htmlCode;
    }
}

class Insert {
    constructor(id) {
        this.id = id;
        this.htmlElement = document.createElement("div");
        this.htmlElement.id = this.id;
        this.htmlElement.classList.add("insert-element");
        this.htmlElement.setAttribute('draggable', true);
        this.htmlElement.addEventListener('dragstart', function(ev) {drag(ev)}, false);

        this.htmlElement.addEventListener("dblclick", function (ev) { 
            drop(ev.target.parentElement.id);
        });

        this.htmlElement.insert = this;
        this.htmlElement.owner = this.getHtmlElement();
        console.log("Insert with ID: " + this.id + " created.");
    }

    getHtmlElement() {
        return this.htmlElement;
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));
    }
}

class ImageInsert extends Insert {
    constructor(id,sourceUrl,thumbnail) {
        super(id);
        this.sourceUrl = sourceUrl;
        this.thumbnail = thumbnail;
        this.imageHtmlElement = document.createElement("img");
        this.imageHtmlElement.style.height = "100%";
        this.imageHtmlElement.style.width = "100%";
        this.imageHtmlElement.src = this.thumbnail;
        this.imageHtmlElement.owner = this.getHtmlElement();
        this.getHtmlElement().appendChild(this.imageHtmlElement);
    }

    setSourceUrl(sourceUrl) {
        this.sourceUrl = sourceUrl;
        this.imageHtmlElement.src = this.sourceUrl;
    }

    getSourceUrl() {
        return this.sourceUrl;
    }

    setThumbnail(thumbnail){
        this.thumbnail = thumbnail;
    }

    getThumbnail(){
        return this.thumbnail;
    }
}

class VideoInsert extends ImageInsert {
    constructor(id,sourceUrl,thumbnail) {
        super(id,sourceUrl,thumbnail);
        this.imageHtmlElement.src = this.thumbnail;
    }
}

class WidgetInsert {
    constructor(name) {
        this.id = this.uuidv4();
        this.name = name;
        this.htmlElement = document.createElement("div");
        this.htmlElement.id = this.id;
        let p = document.createElement("p");
        p.innerHTML = this.name;
        p.addEventListener("dblclick", function (ev) { 
            drop(ev.target.parentElement.id);
        });
        this.htmlElement.appendChild(p);
        this.htmlElement.classList.add("insert-element-widget");
        this.htmlElement.setAttribute('draggable', true);
        this.htmlElement.addEventListener('dragstart', function(ev) {drag(ev)}, false);
        this.htmlElement.addEventListener("dblclick", function (ev) { 
            drop(ev.target.id);
        });
        this.htmlElement.insert = this;
        this.htmlElement.owner = this.getHtmlElement();
    }

    getHtmlElement() {
        return this.htmlElement;
    }

    setName(name) {
        this.name = name;
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));
    }
}

class TextWidgetInsert extends WidgetInsert {
    constructor(name) {
        super(name);
    }
}

class ExternImageWidgetInsert extends WidgetInsert {
    constructor(name) {
        super(name);
    }
}

class ExternVideoWidgetInsert extends WidgetInsert {
    constructor(name) {
        super(name);
    }
}

class TickerWidgetInsert extends WidgetInsert {
    constructor(name) {
        super(name);
    }
}

class QRCodeWidgetInsert extends WidgetInsert {
    constructor(name) {
        super(name);
    }
}

class HTMLWidgetInsert extends WidgetInsert {
    constructor(name) {
        super(name);
    }
}

function deselectAll() {

    document.getElementById("propertyTextArea").style = "display:none;";
    document.getElementById("propertiesPanel").style = "display:none;";

    getWidgets().forEach(element =>{
        element.deselectElement();
        selected = null;
    });
    
    let layerPanel = document.getElementById("layerPanel");
    for (let i = 0; i < layerPanel.children.length; i++) {
        layerPanel.children[i].classList.remove("ui-layer-state-default-selected");
    }

    let editorArea = document.getElementById("editorArea");
    for (let i = 0; i < editorArea.children.length; i++) {
        editorArea.children[i].classList.remove("selected-element");
    }

}

function setDefaultEvents() {

    let addWidgetsEvent = function(arr, callback) {
        arr.push = function(e) {
            Array.prototype.push.call(arr, e);
            callback(arr);
        };
    };

    addWidgetsEvent(getWidgets(), function(newArray) {
        bindElementEvents(newArray[newArray.length - 1]);
        refreshLayerPanel();
    });

    let addInsertElementEvent = function(arr, callback) {
        arr.push = function(e) {
            Array.prototype.push.call(arr, e);
            callback();
        };
    };

    addInsertElementEvent(getInserts(), function() {
        refreshInsertElements();
    });


    $("#layerPanel").sortable({
        placeholder: "ui-state-highlight",
        cursor: 'pointer',
        update: function(event, ui) {
            let layerPanel = document.getElementById("layerPanel");
            let layerPanelElements = layerPanel.children;

            let index = 0;
            for (var i = layerPanelElements.length-1; i >= 0; i--) {
                layerPanelElements[index++].owner.setZIndex(i);
            }
        }
    });

    document.getElementById("editorArea").addEventListener("click", function(element) {
        if(element.target.id == "editorArea") {
            deselectAll();
        }
    });

    document.getElementById("toggleGrid").addEventListener('change', function(event)  {
        if (event.currentTarget.checked) {
            document.getElementById("editorArea").classList.add("grid");
        } else {
            document.getElementById("editorArea").classList.remove("grid");
        }
    });
    let propertyHeight = document.getElementById("propertyHeight");
    let propertyWidth = document.getElementById("propertyWidth");
    let propertyXPosition = document.getElementById("propertyXPosition");
    let propertyYPosition = document.getElementById("propertyYPosition");

    propertyHeight.addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setHeight(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    propertyWidth.addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setWidth(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    propertyXPosition.addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setPositionX(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    propertyYPosition.addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setPositionY(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);


    document.getElementById("propertyFontSize").addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setFontSize(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    document.getElementById("propertyFontWeight").addEventListener("change", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setFontWeight(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    document.getElementById("propertyTickerText").addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setText(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    document.getElementById("propertyHTMLCode").addEventListener("input", function(element) {
        if(selectedElement !== undefined) {
            selectedElement.setHtmlCode(element.target.value);
        }
        else {
            console.log("Select first an element.")
        }
    }, false);

    $('#propertyColor').colorpicker().on('changeColor', function(ev) {
        if(selectedElement !== undefined) {
            selectedElement.setColor(ev.target.value);
            this.style = "border-bottom: 10px solid "+ev.target.value+";"
        }
        else {
            console.log("Select first an element.")
        }
    });

    $('#propertyBackgroundColor').colorpicker().on('changeColor', function(ev) {
        if(selectedElement !== undefined) {
            selectedElement.setBackgroundColor(ev.target.value);
            this.style = "border-bottom: 10px solid "+ev.target.value+";"
        }
        else {
            console.log("Select first an element.")
        }
    });
}

function onInit() {
    //exampleData();
    loadPrimitiveWidgets();
}

function refreshInsertElements() {

    let imagesTab = document.getElementById("imagesTab");
    let videosTab = document.getElementById("videosTab");

    imagesTab.innerHTML = "";
    videosTab.innerHTML = "";

    inserts.forEach((insert) => {
        if(insert.constructor.name === "ImageInsert") {
            imagesTab.appendChild(insert.getHtmlElement());
        }
        else if(insert.constructor.name === "VideoInsert") {
            videosTab.appendChild(insert.getHtmlElement());
        }
    });
}

function loadPrimitiveWidgets(){
    let widgetsTab = document.getElementById("widgetsTab");

    let textWidget = new TextWidgetInsert("Text");
    widgetsTab.appendChild(textWidget.getHtmlElement());

    let externImageWidget = new ExternImageWidgetInsert("Image-URL");
    widgetsTab.appendChild(externImageWidget.getHtmlElement());

    let externVideoWidget = new ExternVideoWidgetInsert("Video-URL");
    widgetsTab.appendChild(externVideoWidget.getHtmlElement());

    let tickerWidget = new TickerWidgetInsert("Ticker");
    widgetsTab.appendChild(tickerWidget.getHtmlElement());

    let qrCodeWidget = new QRCodeWidgetInsert("QR-Code");
    widgetsTab.appendChild(qrCodeWidget.getHtmlElement());

    let htmlWidget = new HTMLWidgetInsert("HTML");
    widgetsTab.appendChild(htmlWidget.getHtmlElement());
}

function allowDrop(ev) {
    ev.preventDefault();
}
  
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.owner.id);
}
  
function drop(ev) {
    
    if(ev.dataTransfer === undefined) {
        // Double click
        var data = ev;
    }
    else {
        //Drag and drop
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
    }
    
    let layerX = Math.round(ev.layerX/10)*10;
    let layerY = Math.round(ev.layerY/10)*10;
    if(data != null && data != "")
    {
        if(document.getElementById(data).owner.insert.constructor.name === "ImageInsert") {
            getWidgets().push(new ImageWidget(undefined, layerX,layerY,defaultWidgetHeight,defaultWidgetWidth,-1,document.getElementById(data).owner.insert.getSourceUrl(), document.getElementById(data).owner.insert.getThumbnail(),{"mediaId":data}));
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "VideoInsert") {
            getWidgets().push(new VideoWidget(undefined, layerX,layerY,defaultWidgetHeight,defaultWidgetWidth,-1,document.getElementById(data).owner.insert.getSourceUrl(), document.getElementById(data).owner.insert.getThumbnail(),{"mediaId":data}));
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "TextWidgetInsert") {
            getWidgets().push(new TextWidget(undefined, layerX,layerY,defaultWidgetHeight,defaultWidgetWidth,-1,"Text", "25",'normal','#000000','transparent'));
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "ExternVideoWidgetInsert") {
            var externVideoWidgetModal = new bootstrap.Modal(document.getElementById('externVideoWidgetModal'), {});
            externVideoWidgetModal.show();
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "ExternImageWidgetInsert") {
            var externImageWidgetModal = new bootstrap.Modal(document.getElementById('externImageWidgetModal'), {});
            externImageWidgetModal.show();
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "TickerWidgetInsert") {
            getWidgets().push(new TickerWidget(undefined, layerX,layerY,defaultWidgetHeight,defaultWidgetWidth,-1,"Ticker", "25",'normal','#000000','transparent',25));
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "QRCodeWidgetInsert") {
            var qrCodeWidgetModal = new bootstrap.Modal(document.getElementById('qrCodeWidgetModal'), {});
            qrCodeWidgetModal.show(); 
        }
        else if(document.getElementById(data).owner.insert.constructor.name === "HTMLWidgetInsert") {
            getWidgets().push(new HTMLWidget(undefined, layerX,layerY,defaultWidgetHeight,defaultWidgetWidth,-1,"Code"));
        }
    }
    else
    {
        console.log("Element not Found.")
    }
}

function bindElementEvents(element) {

    let editorArea = document.getElementById("editorArea");
    editorArea.appendChild(element.getHtmlElement());
    $(element.getHtmlElement()).resizable({
        handles: 'e, s, n, w, nw, ne, se, sw',
        containment: "parent",
        resize: function (e, ui) {
            e.target.widget.setPositionX(ui.position.left);
            e.target.widget.setPositionY(ui.position.top);
            e.target.widget.setHeight(ui.size.height);
            e.target.widget.setWidth(ui.size.width);
            refreshPropertiesPanel(e.target);
        },
        grid: [10, 10]
    });

    var pointerX;
    var pointerY;
    

    $(element.getHtmlElement()).draggable({
        cursor: "move",
        containment: "parent",
        zIndex: 100,
        grid: [10, 10],
        start: function(evt, ui) {
            pointerY = (evt.pageY - $('#editorArea').offset().top) / zoomScale - parseInt($(evt.target).css('top'));
            pointerX = (evt.pageX - $('#editorArea').offset().left) / zoomScale - parseInt($(evt.target).css('left'));
            //console.log('pointer: ' + pointerX + ',' + pointerY);
        },
        drag: function(evt,ui) {
            var canvasTop = $('#editorArea').offset().top;
            var canvasLeft = $('#editorArea').offset().left;
            var canvasHeight = $('#editorArea').height();
            var canvasWidth = $('#editorArea').width();
            //console.log('canvas: ' + canvasLeft + ',' + canvasTop + '  ' + canvasWidth + 'x' + canvasHeight);
            // Fix for zoom
            ui.position.top = Math.round((evt.pageY - canvasTop) / zoomScale - pointerY); 
            ui.position.left = Math.round((evt.pageX - canvasLeft) / zoomScale - pointerX); 
    
            // Check if element is outside canvas

            if (ui.position.left < 0) {
                ui.position.left = 0;
            } 

            if (ui.position.left + $(this).width() > canvasWidth) {
                ui.position.left = canvasWidth - $(this).width();
            }

            if (ui.position.top < 0) {
                ui.position.top = 0;
            }

            if (ui.position.top + $(this).height() > canvasHeight) {
                ui.position.top = canvasHeight - $(this).height();
            }
            // Finally, make sure offset aligns with position
            ui.offset.top = Math.round(ui.position.top + canvasTop);
            ui.offset.left = Math.round(ui.position.left + canvasLeft);

            var snapTolerance = $(this).draggable('option', 'snapTolerance');
            var topRemainder = ui.position.top % 10;
            var leftRemainder = ui.position.left % 10;
            
            if (topRemainder <= snapTolerance) {
                ui.position.top = ui.position.top - topRemainder;
            }
            
            if (leftRemainder <= snapTolerance) {
                ui.position.left = ui.position.left - leftRemainder;
            }


            evt.target.widget.setPositionX(ui.position.left);
            evt.target.widget.setPositionY(ui.position.top);
            refreshPropertiesPanel(evt.target);

        }

    }).dblclick(function () {
        if(this.widget.constructor.name === "TextWidget") {
            // if ($(this).is('.ui-draggable-dragging')) {
            //     return;
            // }
            // //$(this).draggable("option", "disabled", true);
            // //$(this.firstChild).attr('contenteditable', 'true');
            // setTimeout(function(element) {
            //     element.focus();
            // }, 0,this);
        }   
    });
}

function refreshLayerPanel() {
    let layerPanel = document.getElementById("layerPanel");
    layerPanel.innerHTML = '';

    getWidgets().sort((a,b) => {
        return a.zIndex < b.zIndex;
    });

    getWidgets().forEach(element => {
        let li = document.createElement("li");
        li.owner = element;
        li.classList.add("ui-layer-state-default");
        li.textContent = element.constructor.name;
        li.addEventListener("mousedown", function(event){
            deselectAll();
            event.target.owner.selectElement();
            refreshPropertiesPanel(event.target.owner.htmlElement);
        }); 
        layerPanel.appendChild(li);
    });
}

function refreshPropertiesPanel(element) {
    try 
    {
        document.getElementById("propertyTextArea").style = "display:none;";
        document.getElementById("propertyTickerArea").style = "display:none;";
        document.getElementById("propertyHTMLArea").style = "display:none;";
        document.getElementById("propertiesPanel").style = ""

        let propertyHeight = document.getElementById("propertyHeight");
        let propertyWidth = document.getElementById("propertyWidth");
        let propertyXPosition = document.getElementById("propertyXPosition");
        let propertyYPosition = document.getElementById("propertyYPosition");

        propertyHeight.value = element.widget.getHeight();
        propertyWidth.value = element.widget.getWidth();
        propertyXPosition.value = element.widget.getPositionX();
        propertyYPosition.value = element.widget.getPositionY();
        
        if(element.widget.constructor.name == "TextWidget" || element.widget.constructor.name == "TickerWidget") 
        {
            document.getElementById("propertyTextArea").style = "";
            let propertyFontSize = document.getElementById("propertyFontSize");
            let propertyFontWeight = document.getElementById("propertyFontWeight");
            let propertyColor = document.getElementById("propertyColor");
            let propertyBackgroundColor = document.getElementById("propertyBackgroundColor");

            propertyFontSize.value = element.widget.getFontSize();
            propertyFontWeight.value = element.widget.getFontWeight();
            propertyColor.value = element.widget.getColor();
            propertyColor.style = "border-bottom: 10px solid "+element.widget.getColor()+";"
            propertyBackgroundColor.value = element.widget.getBackgroundColor();
            propertyBackgroundColor.style = "border-bottom: 10px solid "+element.widget.getBackgroundColor()+";"
        }

        if(element.widget.constructor.name == "TickerWidget")
        {
            document.getElementById("propertyTickerArea").style = "";

            let propertyTickerText = document.getElementById("propertyTickerText");
            propertyTickerText.value = element.widget.getText();
        }

        if(element.widget.constructor.name == "HTMLWidget") {
            document.getElementById("propertyHTMLArea").style = "";

            let propertyHTMLCode = document.getElementById("propertyHTMLCode");
            propertyHTMLCode.value = element.widget.getHTMLCode();
        }
    } 
    catch (error) 
    {
        console.log(error);
    }
}

$(document).ready(function () {

    setDefaultEvents();
    onInit();
    scaleEditorArea();

    $( window ).resize(function(e) {
        scaleEditorArea();
    });
});

function exampleData() {

    insertMapper('{\r\n        \"inserts\":\r\n        [\r\n            {\r\n                \"id\": \"1\",\r\n                \"src\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n                \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n                \"type\": \"image\"\r\n            },\r\n            {\r\n                \"id\": \"2\",\r\n                \"src\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n                \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n                \"type\": \"image\"\r\n            },\r\n            {\r\n                \"id\": \"3\",\r\n                \"src\": \"http:\/\/commondatastorage.googleapis.com\/gtv-videos-bucket\/sample\/BigBuckBunny.mp4\",\r\n                \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n                \"type\": \"video\"\r\n            },\r\n            {\r\n                \"id\": \"4\",\r\n                \"src\": \"http:\/\/commondatastorage.googleapis.com\/gtv-videos-bucket\/sample\/BigBuckBunny.mp4\",\r\n                \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\",\r\n                \"type\": \"video\"\r\n            }\r\n        ]\r\n    }');
    showInsertPanel();
    showEditor();
}

function insertMapper(jsonString) {
    const jsonObj = JSON.parse(jsonString);
    jsonObj.inserts.forEach((insert => {
        if(insert.type === "image") {
            addImageInsert(insert.id, insert.src, insert.thumbnail);
        }
        else if(insert.type === "video") {
            addVideoInsert(insert.id, insert.src, insert.thumbnail);
        }
    }));
}

function widgetMapper(jsonString) {
    const jsonObj = JSON.parse(jsonString);
    jsonObj.widgets.forEach((widget => {
        if(widget.typeName === "ImageWidget") {
            addWidget(new ImageWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail,{"mediaId":widget.mediaId}));
        }
        else if(widget.typeName === "VideoWidget") {
            addWidget(new VideoWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail),{"mediaId":widget.mediaId});
        }
        else if(widget.typeName === "TextWidget") {
            addWidget(new TextWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.text , widget.fontSize,widget.fontWeight,widget.color,widget.backgroundColor));
        }
        else if(widget.typeName === "TickerWidget") {
            addWidget(new TickerWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.text , widget.fontSize,widget.fontWeight,widget.color,widget.backgroundColor));
        }
        else if(widget.typeName === "HTMLWidget") {
            addWidget(new HTMLWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex,widget.code));
        }
        else if(widget.typeName === "ExternImageWidget") {
            addWidget(new ExternImageWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail,{}));
        }
        else if(widget.typeName === "ExternVideoWidget") {
            addWidget(new ExternVideoWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.sourceUrl , widget.thumbnail),{});
        }
        else if(widget.typeName === "QrCodeWidget") {
            addWidget(new QRCodeWidget(widget.id, widget.positionX, widget.positionY, widget.height, widget.width, widget.zIndex, widget.text));
        }
    }));
}

function getLayoutJsonPayload(title,description) {

    return saveMapper(title,description);
}

function save() { 

    var title = document.getElementById("titleInput");
    removeInvalid(title);

    var description = document.getElementById("descriptionInput");
    removeInvalid(description);

    if(title.value === "") {
        showDangerAlert("Please enter a title.");
        addInvalid(title);
        return false;
    }
    else if(description.value === "") {
        showDangerAlert("Please enter a description.");
        addInvalid(description);
        return false;
    }
    else if(getWidgets().length === 0) {
        showDangerAlert("Please add a screen element.");
        return false;
    }
    getLayoutJsonPayload(title.value,description.value);
}

function saveMapper(title, description) {
    const getCircularReplacer = () => {
    const seen = new WeakSet;
    return (key, value) => {
            if (typeof value === "object" && value !== null) {
                if (seen.has(value)) {
                    return;
                }
                seen.add(value);
            }
            return value;
        };
    };

    let widgetsArray = JSON.parse(JSON.stringify(getWidgets(), getCircularReplacer()));
    widgetsArray.forEach((widgetsArray) => {
        widgetsArray.htmlElement = '';
    });

    var layout = {
        "title": title,
        "description": description,
        "widgets": widgetsArray,
        "height": document.getElementById("editorArea").clientHeight,
        "width": document.getElementById("editorArea").clientWidth
    } 

    
    return layout;
}

function addExternVideo() {
    let url = document.getElementById("externVideoWidgetModalInput");
    removeInvalid(url);
    if(url.value !== "") {
        let videoWidget = new ExternVideoWidget(undefined, 0, 0, defaultWidgetHeight, defaultWidgetWidth, -1, url.value, url.value,{});
        videoWidget.setTypeName("ExternVideoWidget");
        getWidgets().push(videoWidget);
        $('#externVideoWidgetModal').modal('hide');
        url.value = "";
    }
    else {
        addInvalid(url);
        showDangerAlert("Please enter a URL.");
    }
}

function addExternImage() {
    let url = document.getElementById("externImageWidgetModalInput");
    removeInvalid(url);
    if(url.value !== "") {
        let imageWidget = new ExternImageWidget(undefined, 0, 0, defaultWidgetHeight, defaultWidgetWidth, -1, url.value, url.value,{});
        imageWidget.setTypeName("ExternImageWidget");
        getWidgets().push(imageWidget);
        $('#externImageWidgetModal').modal('hide');
        url.value = "";
    }
    else {
        addInvalid(url);
        showDangerAlert("Please enter a URL.");
    }
}

function addQRCode() {
    let qrCodeResponse = document.getElementById("qrCodeWidgetModalInput");
    removeInvalid(qrCodeResponse);
    if(qrCodeResponse.value !== "") {
        getWidgets().push(new QRCodeWidget(undefined, 0,0, defaultWidgetHeight, defaultWidgetWidth,-1,qrCodeResponse.value));
        $('#qrCodeWidgetModal').modal('hide');
        qrCodeResponse.value = "";
    }
    else {
        addInvalid(qrCodeResponse);
        showDangerAlert("Please enter QR Response.");
    }
    
}

function removeElement() {

    let widgetElement = getWidgets().find(element => element.id == selectedElement.id);
    document.getElementById(widgetElement.id).remove();
    for (let i = 0; i < getWidgets().length; i++) {
        if(getWidgets()[i].id == widgetElement.id) {
            getWidgets().splice(i, 1);
            break;
        }
    }

    if(widgetElement.constructor.name == "TickerWidget") {
        widgetElement.clearSlideEffect();
    }

    console.log("Widget with ID: " + widgetElement.id + " removed.");
    refreshLayerPanel();
    deselectAll();
}

function scaleEditorArea() {

    let defaultWidth = 1920;
    let windowWidth = $( window ).width();
    let beautyFactor = 0.3;

    if(windowWidth >= defaultWidth){
        beautyFactor = 0;
    }

    let scale = (windowWidth/defaultWidth) + beautyFactor;
    setZoomScale(scale);
    document.getElementById("editorArea").style.transform = "scale("+getZoomScale()+")";
}

/* UI-Functions */
function showDangerAlert(message) {

    document.getElementById("alertDanger").classList.remove("alert-hide");
    document.getElementById("alertDanger").innerHTML = message;

    setTimeout(function() {
        document.getElementById("alertDanger").classList.add("alert-hide");
    }, 5000);
}

function showSuccessAlert(message) {
    document.getElementById("alertSuccess").classList.remove("alert-hide");
    document.getElementById("alertSuccess").innerHTML = message;

    setTimeout(function() {
        document.getElementById("alertSuccess").classList.add("alert-hide");
    }, 5000);
}

function addValid(element) {
    element.classList.add("is-valid");
}

function removeValid(element) {
    element.classList.add("is-valid");
}

function addInvalid(element) {
    element.classList.add("is-invalid");
}

function removeInvalid(element) {
    element.classList.remove("is-invalid");
}

function showInsertPanel() {
    document.getElementById("insertPanelSpinner").style.display = "none";
    document.getElementById("insertPanel").style.display ="block"; 
}

function hideInsertPanel() {
    document.getElementById("insertPanelSpinner").style.display = "block";
    document.getElementById("insertPanel").style.display ="none"; 
}

function showEditor() {
    document.getElementById("loadingScreen").style.display = "none";
}

function getScreenshotOfElement(element, posX, posY, width, height, callback) {
    html2canvas(element, {
        onrendered: function (canvas) {
            var context = canvas.getContext('2d');
            var imageData = context.getImageData(posX, posY, width, height).data;
            var outputCanvas = document.createElement('canvas');
            var outputContext = outputCanvas.getContext('2d');
            outputCanvas.width = width;
            outputCanvas.height = height;

            var idata = outputContext.createImageData(width, height);
            idata.data.set(imageData);
            outputContext.putImageData(idata, 0, 0);
            callback(outputCanvas.toDataURL().replace("data:image/png;base64,", ""));
        },
        width: width,
        height: height,
        useCORS: true,
        taintTest: false,
        allowTaint: false
    });
}

/**Getter-Setter */

function setZoomScale(scale) {
    zoomScale = scale
}

function getZoomScale() {
    return zoomScale;
}

function getWidgets() {
    return widgets;
}

function addWidget(widget) {
    getWidgets().push(widget);
}

function addImageWidget(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData) {
    addWidget(new ImageWidget(id, positionX, positionY, height, width, zIndex, sourceUrl, thumbnail, metaData));
}

function getInserts() { 
    return inserts;
}

function addImageInsert(id, src, thumbnail) {
    getInserts().push(new ImageInsert(id, src, thumbnail));
}

function addVideoInsert(id, src, thumbnail) {
    getInserts().push(new VideoInsert(id, src, thumbnail));
}