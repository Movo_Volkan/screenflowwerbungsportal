function checkResetPasswordForm()
{

    $("#NewPassword").removeClass("is-invalid");
    $("#NewPasswordRepeat").removeClass("is-invalid");

    var NewPassword =$("#NewPassword").val();
    var NewPasswordRepeat = $("#NewPasswordRepeat").val();
    if(NewPassword == "" || NewPassword.length < 8 || !checkPassword(NewPassword) )
    {
        $("#error_alert").html(languageJsStrings.enterPassword);
        $("#NewPassword").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(NewPasswordRepeat == "" || NewPasswordRepeat.length < 8  || !checkPassword(NewPasswordRepeat))
    {
        $("#error_alert").html(languageJsStrings.enterPassword);
        $("#NewPasswordRepeat").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(NewPassword != NewPasswordRepeat)
    {
        $("#error_alert").html(languageJsStrings.passwordMatchFailed);
        $("#NewPassword").addClass("is-invalid");
        $("#NewPasswordRepeat").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else
    {
        $.post("resetPassword/reset",
            {
                PRC: $("#PRC").val(),
                NewPassword: $("#NewPassword").val(),
                NewPasswordRepeat: $("#NewPasswordRepeat").val()
            })
            .done(function(result){
                $("#resetForm").hide();
                $("#success_alert").html(result.message);
                $("#success_alert").fadeIn();
                setTimeout(function(){ window.location = "./login"; }, 3000);
            })
            .fail(function(result) {
                $("#error_alert").html(result.responseJSON.message);
                $("#error_alert").fadeIn();
            });
    }
    return true;
}