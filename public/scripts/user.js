function editGeneral() {

  $("#success_alert_general").hide();
  $("#error_alert_general").hide();
  $("#firstName").removeClass("is-invalid");
  $("#secondname").removeClass("is-invalid");

  var requiredArray = ["firstName","secondname","billstreet","billhouseNumber","billpostCode","billplace","billfirstname","billhousesecondname","street","houseNumber","postCode","place"];
  if(!checkRequiredInputFields(requiredArray))
  {
    return false;
  }

  if ($("#firstName").val() == "" || $("#firstName").val().length < 2) {
    $("#firstName").addClass("is-invalid");
    $("#error_alert_general").html(languageJsStrings.enterFirstName);
    $("#error_alert_general").fadeIn();
    scrollToTop();
    return false;
  }
  else if ($("#secondname").val() == "" || $("#secondname").val().length < 2) {
    $("#secondname").addClass("is-invalid");
    $("#error_alert_general").html(languageJsStrings.enterName);
    $("#error_alert_general").fadeIn();
    scrollToTop();
    return false;
  }
  else {
    $("#formGeneral").hide();
    $("#saveGeneral").prop('disabled', false);
    $("#general_spinner").fadeIn();
    $.post("/userprofile/editGeneral",
    {
      Salutation: $("#salutation").val(),
      Firstname: $("#billfirstname").val(),
      Secondname: $("#billhousesecondname").val(),

      Street: $("#street").val(),
      HouseNumber: $("#houseNumber").val(),
      Place: $("#place").val(),
      PostCode: $("#postCode").val(),

      BillStreet: $("#billstreet").val(),
      BillHouseNumber: $("#billhouseNumber").val(),
      BillPlace: $("#billplace").val(),
      BillPostCode: $("#billpostCode").val(),
      BillFirstname:$("#firstName").val(),
      BillSecondName:$("#secondname").val()
    })
    .done(function(result){
      $("#general_spinner").fadeOut(1000, function () {
        $("#formGeneral").show();
        $("#success_alert_general").html(result.message);
        $("#success_alert_general").fadeIn();
        setTimeout(function () {
          $("#success_alert_general").fadeOut();
          $("#saveGeneral").prop('disabled', false);
        }, 3000);
      });
    })
    .fail(function(result) {
      $("#general_spinner").fadeOut(1000, function () {
        $("#formGeneral").show();
        $("#error_alert_general").html(result.responseJSON.message);
        $("#error_alert_general").fadeIn();
      });
    });
  }
}

function editAdministration() {
  $("#success_alert_administration").hide();
  $("#error_alert_administration").hide();
  $("#password").removeClass("is-invalid");
  $("#newPassword").removeClass("is-invalid");
  $("#newPasswordRepeat").removeClass("is-invalid");
  $("#newEmail").removeClass("is-invalid");
  $("#newEmailRepeat").removeClass("is-invalid");

  if ($("#password").val() == "" && $("#password").val().length < 8) {
    $("#error_alert_administration").html(languageJsStrings.enterCurrentPassword);
    $("#error_alert_administration").fadeIn();
    $("#password").addClass("is-invalid");
    scrollToTop();
    return false;
  }
  else if ($("#newPassword").val() != $("#newPasswordRepeat").val()) {
    $("#newPassword").addClass("is-invalid");
    $("#newPasswordRepeat").addClass("is-invalid");
    $("#error_alert_administration").html(languageJsStrings.newPasswordNotMatch);
    $("#error_alert_administration").fadeIn();
    scrollToTop();
    return false;
  }
  else if (($("#newPassword").val().length != 0 && $("#newPasswordRepeat").val().length != 0) && (!checkPassword($("#newPassword").val()) || !checkPassword($("#newPasswordRepeat").val()))) {
    $("#newPassword").addClass("is-invalid");
    $("#newPasswordRepeat").addClass("is-invalid");
    $("#error_alert_administration").html(languageJsStrings.enterPassword);
    $("#error_alert_administration").fadeIn();
    scrollToTop();
    return false;
  }
  else if ($("#newEmail").val().length == 0 || $("#newEmailRepeat").val().length == 0) {
    $("#newEmail").addClass("is-invalid");
    $("#newEmailRepeat").addClass("is-invalid");
    $("#error_alert_administration").html(languageJsStrings.enterNewEmail);
    $("#error_alert_administration").fadeIn();
    scrollToTop();
    return false;
  }
  else if (($("#newEmail").val() != $("#newEmailRepeat").val())) {
    $("#newEmail").addClass("is-invalid");
    $("#newEmailRepeat").addClass("is-invalid");
    $("#error_alert_administration").html(languageJsStrings.newPasswordNotMatch);
    $("#error_alert_administration").fadeIn();
    scrollToTop();
    return false;
  }
  else if (!isEmail($("#newEmail").val())) {
    $("#newEmail").addClass("is-invalid");
    $("#newEmailRepeat").addClass("is-invalid");
    $("#error_alert_administration").html(languageJsStrings.emailNotRightFormat);
    $("#error_alert_administration").fadeIn();
    scrollToTop();
    return false;
  }
  else {
    $("#formAdministration").hide();
    $("#saveAdministration").prop('disabled', false);
    $("#administration_spinner").fadeIn();
    $.post("/userprofile/editAdministration",
    {
      Password: $("#password").val(),
      NewPassword: $("#newPassword").val(),
      NewPasswordRepeat: $("#newPasswordRepeat").val(),
      NewEmail: $("#newEmail").val(),
      NewEmailRepeat: $("#newEmailRepeat").val()
    })
    .done(function(result){
      $("#administration_spinner").fadeOut(1000, function () {
        $("#formAdministration").show();
        $("#success_alert_administration").html(result.message);
        $("#success_alert_administration").fadeIn();
        setTimeout(function () {
          $("#success_alert_administration").fadeOut();
          $("#saveAdministration").prop('disabled', false);
        }, 3000);
      });
    })
    .fail(function(result) {
      $("#administration_spinner").fadeOut(1000, function () {
        $("#formAdministration").show();
        $("#error_alert_administration").html(result.responseJSON.message);
        $("#error_alert_administration").fadeIn();
      });
    });
  }
}

function uploadUserProfilePicture() {

  $("#formprofilepicture").hide();
  $("#profilepicture_spinner").fadeIn();
  var fd = new FormData();
  var files = $('#profilPictureUpload')[0].files[0];
  fd.append('fileToUpload', files);

  $.post({
    url: '/userprofile/editProfilePicture',
    data: fd,
    contentType: false,
    processData: false
  })
  .done(function(result){
    $("#profilepicture_spinner").fadeOut(1000, function () {
      $("#formprofilepicture").show();
      $("#success_alert_profilepicture").html(result.message);
      $("#success_alert_profilepicture").fadeIn();
      setTimeout(function () {
        $("#success_alert_profilepicture").fadeOut();
        location.reload();
      }, 1000);
    });
  })
  .fail(function(result) {
    $("#profilepicture_spinner").fadeOut(1000, function () {
      $("#formprofilepicture").show();
      $("#error_alert_profilepicture").html(result.responseJSON.message);
      $("#error_alert_profilepicture").fadeIn();
    });
  });
}

function deleteUserProfilePicture()
{
  $.post({
  url: '/userprofile/removeProfilePicture',
  })
  .done(function(result){
    $("#profilepicture_spinner").fadeOut(1000, function () {
      $("#formprofilepicture").show();
      $("#success_alert_profilepicture").html(result.message);
      $("#success_alert_profilepicture").fadeIn();
      setTimeout(function () {
        $("#success_alert_profilepicture").fadeOut();
        location.reload();
      }, 1000);
    });
  })
  .fail(function(result) {
    $("#profilepicture_spinner").fadeOut(1000, function () {
      $("#formprofilepicture").show();
      $("#error_alert_profilepicture").html(result.responseJSON.message);
      $("#error_alert_profilepicture").fadeIn();
    });
  });
}