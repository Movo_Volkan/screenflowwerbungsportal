function resetPasswordRequest()
{
    $("#resetButton").prop('disabled', true);
    $("#error_alert").fadeOut();
    $("#success_alert").fadeOut();
    var email = $("#Email").val();
    if(email == "" || !isEmail(email))
    {
        $("#error_alert").html(languageJsStrings.enterEmail);
        $("#error_alert").fadeIn();
        $("#Email").addClass("is-invalid");
    }

    $.post("resetPasswordRequest/request",
    {
        EMail: email
    })
    .done(function(result){
        $("#success_alert").html(result.message);
        $("#success_alert").fadeIn();
        $("#resetForm").hide();
    })
    .fail(function(result) {
        $("#error_alert").html(result.responseJSON.message);
        $("#error_alert").fadeIn();
        $("#resetButton").prop('disabled', false);
    });
}