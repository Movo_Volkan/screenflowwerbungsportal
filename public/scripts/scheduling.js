function createScheduling()
{
    $("#success_alert_modal").fadeOut();
    $("#error_alert_modal").fadeOut();

    var ladda = $( '#createSchedule' ).ladda();
    if($("#schedulingName").val().length < 3 || $("#schedulingName").val() == "" )
    {
        $("#error_alert_modal").html(languageJsStrings.enterSchedulName);
        $("#error_alert_modal").fadeIn();
    }
    else if($("#startDate").val() == "")
    {
        $("#error_alert_modal").html(languageJsStrings.enterSchedulStartDate);
        $("#error_alert_modal").fadeIn();
    }
    else if($("#startTime").val() == "")
    {
        $("#error_alert_modal").html(languageJsStrings.enterSchedulStartTime);
        $("#error_alert_modal").fadeIn();
    }
    else
    {
        ladda.ladda( 'start' );
        $("#createSchedule").prop('disabled', true);
        $.post("/scheduling/create",
        {
            SchedulingName: $("#schedulingName").val(),
            SchedulingDescription: $("#schedulingDescription").val(),
            PlayerId: $("#playerId").val(),
            PlaylistId: $("#playlistId").val(),
            StartDate: $("#startDate").val(),
            StartTime: $("#startTime").val(),
            EndDate: $("#endDate").val()
        })
        .done(function(result){
            $("#success_alert_modal").html(result.message);
            $("#success_alert_modal").fadeIn();
            setTimeout(function(){ $("#success_alert_modal").fadeOut(); }, 3000);
            $("#schedulingName").val("");
            $("#schedulingDescription").val("");
            $("#createSchedule").prop('disabled', false);
            $("#addSchedulingModal").on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        })
        .fail(function(result) {
            $("#error_alert_modal").html(result.responseJSON.message);
            $("#error_alert_modal").fadeIn();
            $("#addSchedulingModal").on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        });
    }
}


function editScheduling(index,schedulingId)
{
    var ladda = $( '#editSchedulingButton_'+index ).ladda();
    $("#success_alert_modal_edit_scheduling_"+index).fadeOut();
    $("#error_alert_modal_edit_scheduling_"+index).fadeOut();

    if($("#editSchedulingName_"+index).val().length < 3 || $("#editSchedulingName_"+index).val() == "" )
    {
        $("#error_alert_modal_edit_scheduling_"+index).html(languageJsStrings.enterSchedulName);
        $("#error_alert_modal_edit_scheduling_"+index).fadeIn();
    }
    else if($("#editStartDate_"+index).val() == "")
    {
        $("#error_alert_modal_edit_scheduling_"+index).html(languageJsStrings.enterSchedulStartDate);
        $("#error_alert_modal_edit_scheduling_"+index).fadeIn();
    }
    else if($("#editStartTime_"+index).val() == "")
    {
        $("#error_alert_modal_edit_scheduling_"+index).html(languageJsStrings.enterSchedulStartTime);
        $("#error_alert_modal_edit_scheduling_"+index).fadeIn();
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("/scheduling/edit",
        {
            SchedulingId: schedulingId,
            SchedulingName: $("#editSchedulingName_"+index).val(),
            SchedulingDescription: $("#editSchedulingDescription_"+index).val(),
            PlayerId: $("#editPlayerId_"+index).val(),
            PlaylistId: $("#editPlaylistId_"+index).val(),
            StartDate: $("#editStartDate_"+index).val(),
            StartTime: $("#editStartTime_"+index).val()
        })
        .done(function(result){
            $("#success_alert_modal_edit_scheduling_"+index).html(result.message);
            $("#success_alert_modal_edit_scheduling_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_edit_scheduling_"+index).fadeOut(); }, 3000);
            $("#editSchedulingModal_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        })
        .fail(function(result) {
            $("#error_alert_modal_edit_scheduling_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_edit_scheduling_"+index).fadeIn();
            $("#editSchedulingModal_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        });
    }
}

function deleteScheduling(index,schedulingId)
{
    var ladda = $( '#deleteSchedulingYesButton_'+index ).ladda();
    $("#success_alert_modal_delete_scheduling_"+index).fadeOut();
    $("#error_alert_modal_delete_scheduling_"+index).fadeOut();
    ladda.ladda( 'start' );
    $("#deleteSchedulingYesButton_"+index).prop('disabled', true);
    $("#deleteSchedulingNoButton_"+index).prop('disabled', true);
    $.post("/scheduling/remove",
    {
        SchedulingId: schedulingId
    })
    .done(function(result){
        $("#success_alert_modal_delete_scheduling_"+index).html(result.message);
        $("#success_alert_modal_delete_scheduling_"+index).fadeIn();
        $("#deleteScheduling_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
        ladda.ladda( 'stop' );
        $("#deleteSchedulingYesButton_"+index).prop('disabled', true);
    })
    .fail(function(result) {

        $("#error_alert_modal_delete_scheduling_"+index).html(result.responseJSON.message);
        $("#error_alert_modal_delete_scheduling_"+index).fadeIn();
        $("#deleteScheduling_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
        ladda.ladda( 'stop' );
        $("#deleteSchedulingYesButton_"+index).prop('disabled', true);
    });
}

function showSchedulDeleteModal(index)
{
    $('#editSchedulingModal_'+index).modal('toggle');

    $('#editSchedulingModal_'+index).bind('hidden.bs.modal', function () {
        $('#deleteScheduling_'+index).modal('toggle');
    });
}

$( document ).ready(function() {
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    $('#startDate').attr('min', maxDate);
    $('#endDate').attr('min', maxDate);


    var calendarEl = document.getElementById('calendar');
    var languageSetting = getCookieValue("AppLanguage");
    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [  'interaction', 'dayGrid', 'list' ],
      locale: languageSetting,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridDay,listMonth',
        
      },
      events: dateArray,
      eventClick: function(arg) {
        $('#editSchedulingModal_'+arg.event.extendedProps.index).modal('toggle');
      },
    });
    calendar.on('dateClick', function(info) {

        $("#error_alert").hide();
        var nowDate = new Date;
        var selectedDate = info.date;

        if(!sameDay(nowDate,selectedDate))
        {
            if(Date.now() > selectedDate.getTime())
            {
                $("#error_alert").html(languageJsStrings.onlyFutureDate);
                scrollToTop();
                $("#error_alert").show();
                return false;
            }
        }

        var date = info.dateStr.split('-');
        var timeSplit  = info.dateStr.split('T');
        var languageSetting = getCookieValue("AppLanguage");
        if(languageSetting == "de")
        {
            if(timeSplit.length == 1)
            {
                var dateString = date[2]+"."+date[1]+"."+date[0];
                $("#startDate").val(dateString);
            }
            else if(timeSplit.length == 2)
            {

                var dateSplit = timeSplit[0].split('-');
                var dateString = dateSplit[2]+"."+dateSplit[1]+"."+dateSplit[0];

                var timeSplit = timeSplit[1].split(":");
                var timeString = timeSplit[0]+":"+timeSplit[1];
                $("#startTime").val(timeString);
                $("#startDate").val(dateString);
            }
        }
        $('#addSchedulingModal').modal('toggle');

        
    });
    calendar.render();

    $('.clockpickerStartTime').clockpicker(); 
    $('.clockpickerEndTime').clockpicker(); 

    var languageSetting = getCookieValue("AppLanguage");
    var dateSettings = {
        weekStart: 1,
        language: languageSetting,
        startDate: new Date()
    };
    $('.startDate').datepicker(dateSettings);

    $('.endDate').datepicker(dateSettings);
});

function sameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }