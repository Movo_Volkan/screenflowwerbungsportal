function editPlayer(index,playerId)
{
    var ladda = $( '#editPlayerButton_'+index ).ladda();
    $("#success_alert_modal_editPlayer_"+index).fadeOut();
    $("#error_alert_modal_editPlayer_"+index).fadeOut();
    if($("#playerName_"+index).val().length < 3 || $("#playerName_"+index).val() == "" )
    {
        $("#error_alert_modal_editPlayer_"+index).html(languageJsStrings.enterPlayerName);
        $("#error_alert_modal_editPlayer_"+index).fadeIn();
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("player/edit",
        {
            PlayerId: playerId,
            PlayerName: $("#playerName_"+index).val(),
            PlayerDescription: $("#playerDescription_"+index).val()
        })
        .done(function(result){
            $("#success_alert_modal_editPlayer_"+index).html(result.message);
            $("#success_alert_modal_editPlayer_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_editPlayer_"+index).fadeOut(); }, 3000);
            ladda.ladda( 'stop' );
            $("#editPlayer_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        })
        .fail(function(result) {
            $("#error_alert_modal_editPlayer_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_editPlayer_"+index).fadeIn();
            ladda.ladda( 'stop' );
            $("#editPlayer_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        });
    }
}

function addPlayer(callUrl)
{
    let url = "player/authplayer";
    if(callUrl !== undefined && callUrl !== null && callUrl)
    {
        url = "https://app.screenflow24.com/"+url;
    }
    var ladda = $( '#authPlayer' ).ladda();
    $("#success_alert_modal").fadeOut();
    $("#error_alert_modal").fadeOut();
    $("#newPlayerNameId").removeClass("is-invalid");
    $("#authKey").removeClass("is-invalid");
    if ($("#newPlayerNameId").val().length < 3 || $("#newPlayerNameId").val() == "" )
    {
        $("#error_alert_modal").html(languageJsStrings.enterPlayerName);
        $("#error_alert_modal").fadeIn();
        $("#newPlayerNameId").addClass("is-invalid");
    }
    else if($("#authKey").val().length < 3 || $("#authKey").val() == "" )
    {
        $("#error_alert_modal").html(languageJsStrings.enterAuthenticationKey);
        $("#error_alert_modal").fadeIn();
        $("#authKey").addClass("is-invalid");
    }
    else
    {
        ladda.ladda( 'start' );
        $("#authPlayer").attr("disabled",true);
        $.post(url,
        {
            PlayerKey: $("#authKey").val(),
            PlayerName: $("#newPlayerNameId").val(),
        })
        .done(function(result){
            $("#success_alert_modal").html(result.message);
            $("#success_alert_modal").fadeIn();
            setTimeout(function(){ $("#success_alert_modal").fadeOut(); }, 3000);
            ladda.ladda( 'stop' );
            $("newPlayerNameId").val("");
            $("authKey").val("");
            $("#addPlayer").on('hidden.bs.modal', function () {
                if(callUrl !== undefined && callUrl !== null && callUrl)
                {
                    window.location.href = '/player';
                }
                else
                {
                    location.reload();
                }

            });
        })
        .fail(function(result) {
            $("#error_alert_modal").html(result.responseJSON.message);
            $("#error_alert_modal").fadeIn();
            $("#authPlayer").attr("disabled",false);
            ladda.ladda( 'stop' );
            $("#addPlayer").on('hidden.bs.modal', function () {
                if(callUrl !== undefined && callUrl !== null && callUrl)
                {
                    window.location.href = '/player';
                }
                else
                {
                    location.reload();
                }
            });
        });
    }
}

function deletePlayer(index,playerId)
{
    var ladda = $( '#deletePlayerYesButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#deletePlayerYesButton_"+index).attr("disabled",true);
    $("#error_alert_modal_deletePlayer_"+index).fadeOut();
    $("#success_alert_modal_deletePlayer_"+index).fadeOut();
    $("#deletePlayerNoButton_"+index).attr("disabled",true);
    $.post("/player/removeplayer",
    {
        PlayerId: playerId
    })
    .done(function(result){
        $("#success_alert_modal_deletePlayer_"+index).html(result.message);
        $("#success_alert_modal_deletePlayer_"+index).fadeIn();
        setTimeout(function(){ $("#success_alert_modal").fadeOut(); }, 3000);
        $("#deletePlayer_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
        ladda.ladda( 'stop' );
        $("#deletePlayerYesButton_"+index).attr("disabled",true);
      })
    .fail(function(result) {
        $("#error_alert_modal_deletePlayer_"+index).html(result.responseJSON.message);
        $("#error_alert_modal_deletePlayer_").fadeIn();
        ladda.ladda( 'stop' );
        $("#deletePlayerYesButton_"+index).attr("disabled",true);
    });
}


function removeFromPlayer(index,playerId)
{
    var ladda = $( '#removeFromPlayerYesButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#error_alert_modal_removeFromPlayer_"+index).fadeOut();
    $("#success_alert_modal_removeFromPlayer_"+index).fadeOut();
    $("#removeFromPlayerNoButton_"+index).attr("disabled",true)
    $.post("player/removecontent",
    {
        PlayerId: playerId
    })
    .done(function(result){
        $("#success_alert_modal_removeFromPlayer_"+index).html(result.message);
        $("#success_alert_modal_removeFromPlayer_"+index).fadeIn();
        setTimeout(function(){ $("#success_alert_modal_removeFromPlayer_"+index).fadeOut(); }, 3000);
        $("#removeFromPlayer_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
        ladda.ladda( 'stop' );
        $("#removeFromPlayerYesButton_"+index).attr("disabled",true);
    })
    .fail(function(result) {
        $("#error_alert_modal_removeFromPlayer_"+index).html(result.responseJSON.message);
        $("#error_alert_modal_removeFromPlayer_").fadeIn();
        ladda.ladda( 'stop' );
        $("#removeFromPlayerYesButton_"+index).attr("disabled",true);
    });
}

function deployPlaylistToPlayer(index,playerId)
{
    var ladda = $( '#deployPlaylistToPlayerButton_'+index ).ladda();
    $("#success_alert_modal_deployToPlayer_"+index).fadeOut();
    $("#error_alert_modal_deployToPlayer_"+index).fadeOut();
    if($("#deployToPlayerPlayliste_"+index).val() == "")
    {
        $("#error_alert_modal_deployToPlayer_"+index).html(languageJsStrings.selectPlaylist);
        $("#error_alert_modal_deployToPlayer_"+index).fadeIn();
        $("#deployToPlayerPlayliste_"+index).addClass("is-invalid");
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("player/pushplaylist",
        {
            PlayerId: playerId,
            PlaylistId: $("#deployToPlayerPlayliste_"+index).val(),
        }).done(function(result){
            $("#success_alert_modal_deployToPlayer_"+index).html(result.message);
            $("#success_alert_modal_deployToPlayer_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_deployToPlayer_"+index).fadeOut(); }, 3000);
            ladda.ladda( 'stop' );
            $("#deployToPlayer_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        }).fail(function(result) {
            $("#error_alert_modal_deployToPlayer_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_deployToPlayer_"+index).fadeIn();
            ladda.ladda( 'stop' );
        });
    }
}

function createPlayerGroup() {

    var ladda = $( '#createPlayerGroup' ).ladda();
    
    $("#success_alert_modal_playerGroup").fadeOut();
    $("#error_alert_modal_playerGroup").fadeOut();
    if($("#playerGroupName").val().length < 3 || $("#playerGroupName").val() == "" )
    {
        $("#error_alert_modal_playerGroup").html(languageJsStrings.enterGroupName);
        $("#error_alert_modal_playerGroup").fadeIn();
    }
    else
    {
        ladda.ladda( 'start' );

        $.post("/player/createPlayerGroup",
        {
            Name: $("#playerGroupName").val(),
            Description: $("#playerGroupDescription").val(),
        }).done(function(result)
        {
            $("#success_alert_modal_playerGroup").html(result.message);
            $("#success_alert_modal_playerGroup").fadeIn();
            setTimeout(function(){ $("#success_alert_modal_playerGroup").fadeOut(); }, 3000);
            $("#addPlayerGroup").on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
            disableButton("#createPlayerGroup");
        }).fail(function(result)
        {
            $("#error_alert_modal_playerGroup").html(result.responseJSON.message);
            $("#error_alert_modal_playerGroup").fadeIn();
            ladda.ladda( 'stop' );
        });
    }
}

function deletePlayerGroup(index,groupId)
{
    var ladda = $( '#deletePlayerGroupYesButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#success_alert_modal_deletePlayerGroup_"+index).fadeOut();
    $("#error_alert_modal_deletePlayerGroup_"+index).fadeOut();
    $("#deletePlayerGroupNoButton_"+index).attr("disabled",true);

    $.post("player/deletePlayerGroup",
    {
        GroupID: groupId
    }).done(function(result)
    {
        $("#success_alert_modal_deletePlayerGroup_"+index).html(result.message);
        $("#success_alert_modal_deletePlayerGroup_"+index).fadeIn();
        ladda.ladda( 'stop' );
        disableButton("#deletePlayerGroupYesButton_"+index);
        $("#deletePlayerGroup_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
    }).fail(function(result)
    {
        $("#error_alert_modal_deletePlayerGroup_"+index).html(result.responseJSON.message);
        $("#error_alert_modal_deletePlayerGroup_").fadeIn();
        ladda.ladda( 'stop' );
        $("#deletePlayerGroupYesButton_"+index).attr("disabled",true);
    });
}

function addPlayerToGroup(playerId,groupId)
{
    $.post("player/addPlayerToGroup",
    {
        GroupID: groupId,
        PlayerID: playerId
    }).done(function(result)
    {

    }).fail(function(result)
    {

    });
}

function removePlayerFromGroup(index,playerId)
{
    var ladda = $( '#removePlayerFromGroupYesButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#success_alert_modal_removePlayerFromGroup_"+index).fadeOut();
    $("#error_alert_modal_removePlayerFromGroup_"+index).fadeOut();

    $("#removePlayerFromGroupYesButton_"+index).attr("disabled",true);
    $("#removePlayerFromGroupNoButton_"+index).attr("disabled",true);

    $.post("/playerGroup/removePlayerFromGroup",
    {
        PlayerID: playerId
    }).done(function(result)
    {
        $("#success_alert_modal_removePlayerFromGroup_"+index).html(result.message);
        $("#success_alert_modal_removePlayerFromGroup_"+index).fadeIn();
        $("#removePlayerFromGroup_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
        ladda.ladda( 'stop' );
        disableButton('#removePlayerFromGroupYesButton_'+index);
    }).fail(function(result)
    {
        $("#error_alert_modal_removePlayerFromGroup_"+index).html(result.responseJSON.message);
        $("#error_alert_modal_removePlayerFromGroup_"+index).fadeIn();
    });
}

function editPlayerGroup(index,groupId) 
{
    var ladda = $( '#editPlayerGroupSaveButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#success_alert_modal_editPlayerGroup_"+index).fadeOut();
    $("#error_alert_modal_editPlayerGroup_"+index).fadeOut();
    if($("#editPlayerGroupName_"+index).val().length < 3 || $("#editPlayerGroupName_"+index).val() == "" )
    {
        $("#error_alert_modal_editPlayerGroup_"+index).html(languageJsStrings.enterGroupName);
        $("#error_alert_modal_editPlayerGroup_"+index).fadeIn();
        ladda.ladda( 'stop' );
    }
    else
    {
        $.post("player/editPlayerGroup",
        {
            GroupID: groupId,
            GroupName: $("#editPlayerGroupName_"+index).val(),
            GroupDescription: $("#editPlayerGroupDescription_"+index).val()
        }).done(function(result)
        {
            $("#success_alert_modal_editPlayerGroup_"+index).html(result.message);
            $("#success_alert_modal_editPlayerGroup_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_editPlayerGroup_"+index).fadeOut(); }, 3000);
            ladda.ladda( 'stop' );
            $("#editPlayerGroup_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        }).fail(function(result)
        {
            $("#error_alert_modal_editPlayerGroup_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_editPlayerGroup_"+index).fadeIn();
        });
    }
}

function deployScreenToPlayer(index,playerId)
{
    var ladda = $( '#deployScreenToPlayerButton_'+index ).ladda();
    $("#success_alert_modal_deployToPlayer_"+index).fadeOut();
    $("#error_alert_modal_deployToPlayer_"+index).fadeOut();
    if($("#deployToPlayerScreen_"+index).val() == "")
    {
        $("#error_alert_modal_deployToPlayer_"+index).html(languageJsStrings.selectScreenToShow);
        $("#error_alert_modal_deployToPlayer_"+index).fadeIn();
        $("#deployToPlayerScreen_"+index).addClass("is-invalid");
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("player/pushscreen",
        {
            PlayerId: playerId,
            ScreenId: $("#deployToPlayerScreen_"+index).val(),
        }).done(function(result){
            $("#success_alert_modal_deployToPlayer_"+index).html(result.message);
            $("#success_alert_modal_deployToPlayer_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_deployToPlayer_"+index).fadeOut(); }, 3000);
            $("#deployToPlayer_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        }).fail(function(result) {
            $("#error_alert_modal_deployToPlayer_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_deployToPlayer_"+index).fadeIn();
            ladda.ladda( 'stop' );
        });
    }
}

function deployScreenToPlayerGroup(index,groupId)
{
    var ladda = $( '#deployScreenToPlayerGroupButton_'+index ).ladda();
    $("#success_alert_modal_deployToPlayerGroup_"+index).fadeOut();
    $("#error_alert_modal_deployToPlayerGroup_"+index).fadeOut();
    if($("#deployToPlayerGroupScreen_"+index).val() == "")
    {
        $("#error_alert_modal_deployToPlayerGroup_"+index).html(languageJsStrings.selectScreenToShow);
        $("#error_alert_modal_deployToPlayerGroup_"+index).fadeIn();
        $("#deployToPlayerGroupScreen_"+index).addClass("is-invalid");
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("player/deployScreenToPlayerGroup",
        {
            GroupID: groupId,
            ScreenID: $("#deployToPlayerGroupScreen_"+index).val(),
        }).done(function(result){
            $("#success_alert_modal_deployToPlayerGroup_"+index).html(result.message);
            $("#success_alert_modal_deployToPlayerGroup_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_deployToPlayerGroup_"+index).fadeOut(); }, 3000);
            $("#deployToPlayer_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        }).fail(function(result) {
            $("#error_alert_modal_deployToPlayerGroup_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_deployToPlayerGroup_"+index).fadeIn();
        });
    }

}

function deployPlaylistToPlayerGroup(index,groupId)
{
    var ladda = $( '#deployPlaylistToPlayerGroupButton_'+index ).ladda();
    $("#success_alert_modal_deployToPlayerGroup_"+index).fadeOut();
    $("#error_alert_modal_deployToPlayerGroup_"+index).fadeOut();
    if($("#deployToPlayerPlaylisteGroup_"+index).val() == "")
    {
        $("#error_alert_modal_deployToPlayerGroup_"+index).html(languageJsStrings.selectPlaylist);
        $("#error_alert_modal_deployToPlayerGroup_"+index).fadeIn();
        $("#deployToPlayerGroupPlayliste_"+index).addClass("is-invalid");
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("player/deployPlaylistToPlayerGroup",
        {
            GroupID: groupId,
            PlaylistID: $("#deployToPlayerGroupPlayliste_"+index).val(),
        }).done(function(result){

            $("#success_alert_modal_deployToPlayerGroup_"+index).html(result.message);
            $("#success_alert_modal_deployToPlayerGroup_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_deployToPlayerGroup_"+index).fadeOut(); }, 3000);
            $("#deployToPlayer_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
            ladda.ladda( 'stop' );
        }).fail(function(result) {
            $("#error_alert_modal_deployToPlayerGroup_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_deployToPlayerGroup_"+index).fadeIn();
        });
    }

}

function togglePushButtons(tabName,index)
{
    if(tabName == "screen")
    {
        $("#deployScreenToPlayerButton_"+index).show();
        $("#deployPlaylistToPlayerButton_"+index).hide();
    }
    else if(tabName == "playlist")
    {
        $("#deployScreenToPlayerButton_"+index).hide();
        $("#deployPlaylistToPlayerButton_"+index).show();
    }
}

function togglePushButtonsGroup(tabName,index)
{
    if(tabName == "screen")
    {
        $("#deployScreenToPlayerGroupButton_"+index).show();
        $("#deployPlaylistToPlayerGroupButton_"+index).hide();
    }
    else if(tabName == "playlist")
    {
        $("#deployScreenToPlayerGroupButton_"+index).hide();
        $("#deployPlaylistToPlayerGroupButton_"+index).show();
    }
}

function removeFromPlayerGroup(index,groupId)
{
    var ladda = $( '#removeFromPlayerGroupYesButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#error_alert_modal_removeFromPlayerGroup_"+index).fadeOut();
    $("#success_alert_modal_removeFromPlayerGroup_"+index).fadeOut();

    $.post("player/removeContentFromPlayerGroup",
    {
        GroupID: groupId
    })
    .done(function(result)
    {
        $("#success_alert_modal_removeFromPlayerGroup_"+index).html(result.message);
        $("#success_alert_modal_removeFromPlayerGroup_"+index).fadeIn();
        setTimeout(function(){ $("#success_alert_modal_removeFromPlayerGroup_"+index).fadeOut(); }, 3000);
        $("#removeFromPlayerGroup_"+index).on('hidden.bs.modal', function () {
            location.reload();
        });
        ladda.ladda( 'stop' );
    })
    .fail(function(result) {
        $("#error_alert_modal_removeFromPlayerGroup_"+index).html(result.responseJSON.message);
        $("#error_alert_modal_removeFromPlayerGroup_").fadeIn();

        ladda.ladda( 'stop' );
        $("#removeFromPlayerGroupYesButton_"+index).attr("disabled",true);
    });
}