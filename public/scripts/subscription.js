function cancelOrder(paymentType,subsciptionID)
{
  $("#error_alert_cancel_modal").hide();
  $("#success_alert_cancel_modal").hide();
  $('#cancelOrderSubscription').prop('disabled', true);
  $('#cancelOrderSubscriptionNo').prop('disabled', true);
  var ladda = $( '#cancelOrderSubscription' ).ladda();
  ladda.ladda( 'start' );
  $("#cancelSubscription").on('hidden.bs.modal', function () {
    location.reload(); 
  });
  $.post("./functions/order.php", {
    Function: "CancelSubscription"
  },
  function(result) {
    try 
    {
      
      var jsonResult = JSON.parse(result);
      if(jsonResult.status == "successfull") 
      {
        $("#success_alert_cancel_modal").show();
        $("#success_alert_cancel_modal").html(jsonResult.message);
      }
      else if(jsonResult.status == "error")
      {
        $("#error_alert_cancel_modal").show();
        $("#error_alert_cancel_modal").html(jsonResult.message);
      }
    }
    catch (exception) 
    {
      $("#error_alert_cancel_modal").show();
      $("#error_alert_cancel_modal").html(exception);

    }
    ladda.ladda( 'stop' );
    $('#cancelOrderSubscription').prop('disabled', true);
  });
}

function cancelSubscription()
{
  var ladda = $( '#cancelOrderSubscription' ).ladda();
  ladda.ladda( 'start' );
  $("#cancelSubscription").on('hidden.bs.modal', function () {
    location.reload();
  });
  $.post("/order/cancelSubscription",
  {

  })
  .done(function(result){
    $("#success_alert_cancel_modal").html(result.message);
    $("#success_alert_cancel_modal").fadeIn();
    ladda.ladda( 'stop' );
    $("#cancelOrderSubscription").hide();
    $("#cancelOrderSubscriptionNo").hide();

  })
  .fail(function(result) {
    $("#error_alert_cancel_modal").html(result.responseJSON.message);
    $("#error_alert_cancel_modal").fadeIn();
    ladda.ladda( 'stop' );
    $("#cancelOrderSubscription").hide();
    $("#cancelOrderSubscriptionNo").hide();
  });
}