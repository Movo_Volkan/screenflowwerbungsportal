var selectedPlanId = -1;
var numberOfPlayerToOrder = -1;
var paymentid = -1;

$(document).ready(function() {

    var submitCardButton;

    $("#orderSteps").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        labels: {
            finish: languageJsStrings.toOrder,
            next: languageJsStrings.continue,
            previous: languageJsStrings.back,
            loading: languageJsStrings.loading
        },
       
        onStepChanged: function(e, currentIndex, newIndex) {

           if(currentIndex == 2)
            {
                var stripeElements = function(publicKey, setupIntent) {
                    var stripe = Stripe(publicKey);
                    var elements = stripe.elements();

                    // Element styles
                    var style = {
                        base: {
                            fontSize: "16px",
                            color: "#32325d",
                            fontFamily:
                                "-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, sans-serif",
                            fontSmoothing: "antialiased",
                            "::placeholder": {
                                color: "rgba(0,0,0,0.4)"
                            }
                        }
                    };

                    var card = elements.create("card", { style: style });

                    card.mount("#card-element");

                    // Element focus ring
                    card.on("focus", function() {
                        var el = document.getElementById("card-element");
                        el.classList.add("focused");
                    });

                    card.on("blur", function() {
                        var el = document.getElementById("card-element");
                        el.classList.remove("focused");
                    });

                    // Handle payment submission when user clicks the pay button.
                    var button = document.getElementById("submit");
                    button.addEventListener("click", function(event) {
                        event.preventDefault();
                        changeLoadingState(true);
                        var email = document.getElementById("email").value;

                        stripe.confirmCardSetup(setupIntent.client_secret, {
                            payment_method: {
                                card: card,
                                billing_details: { email: email }
                            }
                        })
                        .then(function(result) {
                            if (result.error) {
                                changeLoadingState(false);
                                var displayError = document.getElementById("card-errors");
                                displayError.textContent = result.error.message;
                            } else {
                                // The PaymentMethod was successfully set up
                                orderComplete(stripe, setupIntent.client_secret);
                            }
                        });
                    });
                };

                var getSetupIntent = function(publicKey) {
                    return fetch("/order/createStripeSetupIntent", {
                        method: "post",
                        headers: {
                            "Content-Type": "application/json"
                        }
                    })
                        .then(function(response) {
                            return response.json();
                        })
                        .then(function(setupIntent) {
                            stripeElements(publicKey, setupIntent);
                        });
                };

                /* Shows a success / error message when the payment is complete */
                var orderComplete = function(stripe, clientSecret) {
                    stripe.retrieveSetupIntent(clientSecret).then(function(result) {
                        var setupIntent = result.setupIntent;
                        var setupIntentJson = JSON.stringify(setupIntent, null, 2);

                        document.querySelector(".sr-payment-form").classList.add("hidden");
                        document.querySelector(".sr-result").classList.remove("hidden");
                        document.querySelector("pre").textContent = setupIntentJson;
                        setTimeout(function() {
                            document.querySelector(".sr-result").classList.add("expand");
                        }, 200);

                        changeLoadingState(false);
                    });
                };

                getSetupIntent($("#StripePublishableKey").val());

            }
        },
        onStepChanging: function (event, currentIndex, newIndex){
            if (currentIndex > newIndex)
            {
                return true;
            }
       
            $("#orderFailedAlert").hide();
            if(currentIndex == 0)
            {
                if($("#numberOfPlayers").val() == "" || isNaN(parseInt($("#numberOfPlayers").val())))
                {
                    $("#orderFailedAlert").html(""+languageJsStrings.enterNumberOfPlayer);
                    $("#orderFailedAlert").show();
                    return false;
                }
                numberOfPlayerToOrder = $("#numberOfPlayers").val();
            }
            else if(currentIndex == 1)
            {
                var billAddressFields = ["billfirstname","billSecondName","billstreet","billhouseNumber","billplace","billpostCode"];

                for (let i = 0; i < billAddressFields.length; i++) {

                    if($("#"+billAddressFields[i]).val() == "")
                    {
                        $("#"+billAddressFields[i]).addClass("is-invalid");
                        $("#orderFailedAlert").html(""+languageJsStrings.fillBillAddresses);
                        $("#orderFailedAlert").show();
                        return false;
                    }
                }
            }
            return true;
        },
        onFinishing: function (event, currentIndex)
        {
            if (!$('#acceptTerm').prop('checked')) {
                $("#acceptTerm").addClass("is-invalid");
                $("#orderFailedAlert").html(""+languageJsStrings.pleaseAcceptTerms);
                $("#orderFailedAlert").show();
                return false;
            }
            return true;
        },
        onFinished: function (event, currentIndex)
        {
            var form = $("#orderFrom");
            form.submit();
        }
    });

    $("#deviantBillingAddress").change(function (e) {
        if(this.checked) {
            $("#billingAddressArea").show();
            $("#billingAddress").val("true");
        }
        else
        {
            $("#billingAddressArea").hide();
            $("#billingAddress").val("false");
        }
    });

    $("#numberOfPlayers").keyup(function (e) { 

        if (/^\d+$/.test($(this).val())) {
            
        } 
        else 
        {
            console.log("char");
            this.value = 1;
        }
    });

    $("#numberOfPlayers").blur(function () { 
        if(parseInt(this.value) < 1)
        {
            decreaseNumberOfPlayer();
        }
    });

    $(".paymentBox").click(function (e) { 
        $(".paymentBox").removeClass("planIsSelected");
        e.preventDefault();
        $(this).addClass("planIsSelected");
        paymentid = this.dataset.paymentid;
    });

    $("#privateCustomer").click(function (){
        $("#firstnameLabel").html(languageJsStrings.firstName);
        $("#secondNameLabel").html(languageJsStrings.secondName);

        $("#billfirstnameLabel").html(languageJsStrings.firstName);
        $("#billSecondNameLabel").html(languageJsStrings.secondName);
        $("#customerType").val("private");
    })

    $("#businessCustomer").click(function (){
        $("#firstnameLabel").html(languageJsStrings.company);
        $("#secondNameLabel").html(languageJsStrings.contactPerson);

        $("#billfirstnameLabel").html(languageJsStrings.company);
        $("#billSecondNameLabel").html(languageJsStrings.contactPerson);
        $("#customerType").val("business");
    })


});

function increaseNumberOfPlayer()
{
    var value = $("#numberOfPlayers").val();
    $("#numberOfPlayers").val(parseInt(value)+1);
}

function decreaseNumberOfPlayer()
{
    var value = $("#numberOfPlayers").val();
    value = parseInt(value)-1;
    if(value < 1 )
    {
        value = 1;
    }
    $("#numberOfPlayers").val(value);
}