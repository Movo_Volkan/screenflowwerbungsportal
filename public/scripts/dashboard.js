function submitContact() {
  $("#ContactSuccess").hide();
  $("#ContactDanger").hide();
  $.ajax({
    url: "./functions/dashboard.php?Function=SubmitContact",
    type: "post",
    dataType: "json",
    data: $("#quickContact").serialize(),
    success: function (result) {
      $("#ContactMessage,#submitContactButton").fadeOut("slow", function () {});
      if (result.status == "successfull") {
        $("#ContactSuccess").show();
        $("#ContactSuccess").html(
          result.message + " " + languageJsStrings.backToContactForm
        );
      } else {
        $("#ContactDanger").show();
        $("#ContactDanger").html(
          result.message + " " + languageJsStrings.backToContactForm
        );
      }
    },
  });
}
