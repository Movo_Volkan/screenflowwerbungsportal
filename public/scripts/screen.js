function deleteScreen(index, screenId) {
    var ladda = $('#deleteScreenYesButton_' + index).ladda();
    $("#success_alert_modal_delete_screen_" + index).fadeOut();
    $("#error_alert_modal_delete_screen_" + index).fadeOut();

    $("#deleteScreenYesButton_" + index).prop('disabled', true);
    $("#deleteScreenNoButton_" + index).prop('disabled', true);
    ladda.ladda('start');

    httpDelete("screens/"+screenId,function (result){
        console.log(result);
        ladda.ladda('stop');
        $("#success_alert_modal_delete_screen_" + index).html(result.message);
        $("#success_alert_modal_delete_screen_" + index).fadeIn();
        $("#deleteScreenYesButton_" + index).prop('disabled', true);
        $("#deleteScreenNoButton_" + index).prop('disabled', true);
        $("#deleteScreen_" + index).on('hidden.bs.modal', function () {
            location.reload();
        });
    });
}
