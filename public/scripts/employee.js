function addUserAsEmployee()
{
    var ladda = $( '#addUserAsEmployeeId' ).ladda();
    $("#error_alert_modal").hide();
    $("#error_alert_modal").hide();
    if ($("#newUserForEmployeeEmail").val().length < 3 || $("#newUserForEmployeeEmail").val() == "")
    {
        $("#error_alert_modal").html(languageJsStrings.enterEmailAdressOfEmployee);
        $("#error_alert_modal").fadeIn();
        $("#newUserForEmployeeEmail").addClass("is-invalid");
    }
    else
    {
        ladda.ladda( 'start' );
        $.post("./functions/employee.php?Function=AddUserAsEmployee",
        {
            UserEmail: $("#newUserForEmployeeEmail").val(),
        },
        function(result)
        {
            try
            {
                var jsonResult = JSON.parse(result);
                if(jsonResult.status == "successfull")
                {
                    $("#success_alert_modal").html(jsonResult.message);
                    $("#success_alert_modal").fadeIn();
                    setTimeout(function(){ $("#success_alert_modal").fadeOut(); }, 3000);
                }
                else if(jsonResult.status == "error")
                {
                    $("#error_alert_modal").html(jsonResult.message);
                    $("#error_alert_modal").fadeIn();
                }
                $("#addEmployee").on('hidden.bs.modal', function () {
                    location.reload();
                });
                ladda.ladda( 'stop' );
            }
            catch (exception) 
            {
                $("#error_alert_modal").html(exception + "Result: " + result);
                $("#error_alert_modal").fadeIn();
                $("#addEmployee").on('hidden.bs.modal', function () {
                    location.reload();
                });
            }
        });
    }
}

function deleteUserEmployee(index,employeeId)
{
    var ladda = $( '#deleteUserEmployeeYesButton_'+index ).ladda();
    ladda.ladda( 'start' );
    $("#error_alert_modal_deleteUserEmployee_"+index).fadeOut();
    $("#success_alert_modal_deleteUserEmployee_"+index).fadeOut();
    $("#deleteUserEmployeeNoButton_"+index).attr("disabled",true)
    $.post("./functions/employee.php?Function=DeleteUserEmployee",
    {
        EmployeeId: employeeId
    },
    function(result)
    {   
        try
        {
            var jsonResult = JSON.parse(result);
            if(jsonResult.status == "successfull")
            {
                $("#success_alert_modal_deleteUserEmployee_"+index).html(jsonResult.message);
                $("#success_alert_modal_deleteUserEmployee_"+index).fadeIn();
                setTimeout(function(){ $("#success_alert_modal_deleteUserEmployee_"+index).fadeOut(); }, 3000);
                $("#deleteUserEmployee_"+index).on('hidden.bs.modal', function () {
                    location.reload(); 
                });
            }
            else if(jsonResult.status == "error")
            {
                $("#error_alert_modal_deleteUserEmployee_"+index).html(jsonResult.message);
                $("#error_alert_modal_deleteUserEmployee_").fadeIn();
            }
        }
        catch (exception) 
        {
            $("#error_alert_modal_deleteUserEmployee_"+index).html(exception + "Result: " + result);
            $("#error_alert_modal_deleteUserEmployee_"+index).fadeIn();
            $("#deleteUserEmployee_"+index).on('hidden.bs.modal', function () {
                location.reload(); 
            });
        }
        ladda.ladda( 'stop' );
        $("#deleteUserEmployeeYesButton_"+index).attr("disabled",true);
    });
}

function setRights(element,right,employeeId)
{
    
    $.post("./functions/employee.php?Function=SetEmployeeRight",
    {
        EmployeeId: employeeId,
        EmployeeRight:right,
        EmployeeChecked:element.checked
    },
    function(result)
    {   
        try
        {
            var jsonResult = JSON.parse(result);
        }
        catch (exception) 
        {
           
        }
    });
}