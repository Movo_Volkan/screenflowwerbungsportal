function showPlayerTour()
{
    var tourCookie = getCookieValue("playerTour");
    
    if(tourCookie == "" && tourCookie != "true")
    {
        const driver = new Driver({
            opacity: 0,
            onReset: (Element) => {
              setTourCookie("playerTour");
            },
               
            doneBtnText: languageJsStrings.finish,
            closeBtnText: languageJsStrings.closing,
            nextBtnText: languageJsStrings.continue,
            prevBtnText: languageJsStrings.back,
            allowClose: false,
        });
        // Define the steps for introduction
        driver.defineSteps([
          {
            element: '#addPlayerButton',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString1,
              description: languageJsStrings.tourString1,
              position: 'left'
            }
          },
        ]);
        // Start the introduction
        driver.start();
    }
}

function showCreateScreenTour()
{
    var tourCookie = getCookieValue("createScreenTour")

    if(tourCookie == "" && tourCookie != "true")
    {
        const driver = new Driver({
            opacity: 0,
            onReset: (Element) => {
              setTourCookie("createScreenTour");
            },
               
            doneBtnText: languageJsStrings.finish,
            closeBtnText: languageJsStrings.closing,
            nextBtnText: languageJsStrings.continue,
            prevBtnText: languageJsStrings.back,
            allowClose: false,
        });
        // Define the steps for introduction
        driver.defineSteps([
          {
            element: '#siteName',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString2,
              description: languageJsStrings.tourString2,
              position: 'bottom'
            }
            
          },
          {
            element: '#screenName',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString3,
              description: languageJsStrings.tourString3,
              position: 'bottom'
            }
            
          },
          {
            element: '#screenDescription',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString4,
              description: languageJsStrings.tourString4,
              position: 'bottom'
            }
          },
          {
            element: '#previewContainer',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString5,
              description: languageJsStrings.tourString5,
              position: 'top'
            }
          },
          {
            element: '#mediaZone',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString6,
              description: languageJsStrings.tourString6,
              position: 'top'
            }
          },
          {
            element: '#layoutManage',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString7,
              description: languageJsStrings.tourString7,
              position: 'right'
            }
          }
        ]);
        driver.start();
    }
}

function showCreatePlaylistTour()
{
    var tourCookie = getCookieValue("playlistTour")

    if(tourCookie == "" && tourCookie != "true")
    {
        const driver = new Driver({
            opacity: 0,
            onReset: (Element) => {
              setTourCookie("playlistTour");
            },
               
            doneBtnText: languageJsStrings.finish,
            closeBtnText: languageJsStrings.closing,
            nextBtnText: languageJsStrings.continue,
            prevBtnText: languageJsStrings.back,
            allowClose: false,
        });
        // Define the steps for introduction
        driver.defineSteps([
          {
            element: '#siteName',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString8,
              description: languageJsStrings.tourString8,
              position: 'bottom'
            }
          },
          {
            element: '#screenList',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString9,
              description: languageJsStrings.tourString9,
              position: 'bottom'
            }
          },
        ]);
        // Start the introduction
        driver.start();
    }
}

function showFirstPlaylistItemTour()
{
  var tourCookie = getCookieValue("playlistItemTour")

    if(tourCookie == "" && tourCookie != "true")
    {
        const driver = new Driver({
            opacity: 0,
            onReset: (Element) => {
              setTourCookie("playlistItemTour");
            },
               
            doneBtnText: languageJsStrings.finish,
            closeBtnText: languageJsStrings.closing,
            nextBtnText: languageJsStrings.continue,
            prevBtnText: languageJsStrings.back,
            allowClose: false,
        });
        // Define the steps for introduction
        driver.defineSteps([
          {
            element: '#siteName',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString8,
              description: languageJsStrings.tourString10,
              position: 'bottom'
            }
          }
        ]);
        // Start the introduction
        driver.start();
    }
}

function firstStartTour()
{
    var tourCookie = getCookieValue("firstStartTour")

    if(tourCookie == "" && tourCookie != "true")
    {
        const driver = new Driver({
            opacity: 0,
            onReset: (Element) => {
              setTourCookie("firstStartTour");
            },
            doneBtnText: languageJsStrings.finish,
            closeBtnText: languageJsStrings.closing,
            nextBtnText: languageJsStrings.continue,
            prevBtnText: languageJsStrings.back,
            allowClose: false,
        });
        // Define the steps for introduction
        driver.defineSteps([
          {
            element: '#dashboardMenuItem',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString11,
              description: languageJsStrings.tourString11,
              position: 'right'
            }
          },
          {
            element: '#playerMenuItem',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString15,
              description: languageJsStrings.tourString15,
              position: 'right'
            }
          },
          {
            element: '#mediaMenuItem',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString12,
              description: languageJsStrings.tourString12,
              position: 'right'
            }
          },
          {
            element: '#screenMenuItem',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString13,
              description: languageJsStrings.tourString13,
              position: 'right'
            }
          },
          {
            element: '#playlistMenuItem',
            popover: {
              className: 'first-step-popover-class',
              title: languageJsStrings.titleTourString14,
              description: languageJsStrings.tourString14,
              position: 'right'
            }
          },
        ]);
        // Start the introduction
        driver.start();
    }
}

function restartTour()
{
    document.cookie = "playerTour=;";
    document.cookie = "createScreenTour=;";
    document.cookie = "playlistTour=;";
    document.cookie = "firstStartTour=;";
}

function setTourCookie(name)
{
  var d = new Date();
  d.setTime(d.getTime() + 10*365*24*60*60*1000);
  document.cookie = name+'=true;expires='+d.toGMTString()+';'; 
}