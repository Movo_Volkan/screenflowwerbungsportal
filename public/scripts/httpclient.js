function httpPost(url,body, success, error){
    $.ajax({
        data: {
            "body": body
        },
        url: url,
        type: 'POST',
        success: success,
        error: error,
        cache: false
    });
}

function httpPostJson(url,body, success, error){
    $.ajax({
        data: {
            "body": body
        },
        dataType: 'json',
        url: url,
        type: 'POST',
        success: success,
        error: error,
        cache: false
    });
}

function httpGet(url, success, error){
    $.ajax({
        url: url,
        type: 'GET',
        success: success,
        error: error,
        cache: false
    });
}

function httpPutJson(url,body, success, error){
    $.ajax({
        data: {
            "body": body
        },
        dataType: 'json',
        url: url,
        type: 'PUT',
        success: success,
        error: error,
        cache: false
    });
}

function httpDelete(url, success, error) {
    $.ajax({
        url: url,
        type: 'DELETE',
        success: success,
        error: error,
        cache: false
    });
}
