var formIsSubmitted = false;
function checkRegisterForm()
{
    $("#success_alert").fadeOut();
    $("#error_alert").fadeOut();

    $("#serverMessageSuccess").fadeOut();
    $("#serverMessageError").fadeOut();
    
    $("#user_salutation").removeClass("is-invalid");
    $("#user_firstname").removeClass("is-invalid");
    $("#user_secondname").removeClass("is-invalid");
    $("#user_email").removeClass("is-invalid");

    $("#user_plainPassword_first").removeClass("is-invalid");
    $("#user_plainPassword_second").removeClass("is-invalid");
    $("#AGB").removeClass("is-invalid");

    var firstname = $("#user_firstname").val();
    var secondname =$("#user_secondname").val();
    var email =$("#user_email").val();

    var password =$("#user_plainPassword_first").val();
    var passwordRepeat = $("#user_plainPassword_second").val();
    var agb = $('#AGB').is(':checked');
    var salutation = $("#user_salutation").val();

    if(salutation == null)
    {
        $("#error_alert").html(languageJsStrings.selectSalutation);
        $("#user_salutation").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(firstname == "" || firstname.length < 2 )
    {
        $("#error_alert").html(languageJsStrings.enterFirstName);
        $("#user_firstname").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(secondname == "" || secondname.length < 2)
    {
        $("#error_alert").html(languageJsStrings.enterName);
        $("#user_secondname").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(email == "" || email.length < 3 || !isEmail(email))
    {
        $("#error_alert").html(languageJsStrings.enterEmail);
        $("#user_email").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(password == "" || password.length < 8 || !checkPassword(password))
    {
        $("#error_alert").html(languageJsStrings.enterPassword);
        $("#user_plainPassword_first").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(passwordRepeat == "" || passwordRepeat.length < 8  || !checkPassword(passwordRepeat))
    {
        $("#error_alert").html(languageJsStrings.enterPassword);
        $("#user_plainPassword_second").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(passwordRepeat != password)
    {
        $("#error_alert").html(languageJsStrings.passwordMatchFailed);
        $("#user_plainPassword_first").addClass("is-invalid");
        $("#user_plainPassword_second").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(!agb)
    {
        $("#error_alert").html(languageJsStrings.acceptAgb);
        $("#AGB").addClass("is-invalid");
        $("#error_alert").fadeIn();
        return false;
    }

    if(!formIsSubmitted)
    {
        return checkIsEmailExist(email);
    }

}

function checkIsEmailExist(email)
{
    $.post("/register/checkIsEmailExist",
    {
        Email: email,
    })
    .done(function(result){
        $("#error_alert").html(languageJsStrings.emailIsNotAvailable);
        $("#user_email").addClass("is-invalid");
        $("#error_alert").fadeIn();
    })
    .fail(function(result) {
        formIsSubmitted = true;
        $( "#registerForm" ).submit();
    });

    return false;
}