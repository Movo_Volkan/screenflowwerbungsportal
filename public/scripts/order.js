var selectedPlanId = -1;
var numberOfPlayerToOrder = -1;
var paymentid = -1;

$(document).ready(function() {
    $("#orderSteps").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        labels: {
            finish: languageJsStrings.toOrder,
            next: languageJsStrings.continue,
            previous: languageJsStrings.back,
            loading: languageJsStrings.loading
        },
       
        onStepChanged: function(e, currentIndex, newIndex) {

            if(currentIndex == 4)
            {
               $("#Quantity").val(numberOfPlayerToOrder);
               $("#SubscriptionPlanid").val(selectedPlanId);
               $("#Paymentid").val(paymentid);

                const formatter = new Intl.NumberFormat('en-US', {
                    minimumFractionDigits: 2,      
                    maximumFractionDigits: 2,
                });

               for (let i = 0; i < plans.length; i++) {
                   if(plans[i].PlanID == selectedPlanId)
                   {
                        var pricePerMonth = plans[i].PlanPricePerMonth*numberOfPlayerToOrder;
                        pricePerMonth = Number(Math.round(pricePerMonth+'e'+2)+'e-'+2);
                        var pricePerMonthText = formatter.format(pricePerMonth);
                        var planPerMonthText = plans[i].PlanPricePerMonth.toString().replace(".", ",");
                        $("#priceOutput").html(numberOfPlayerToOrder + " x " + planPerMonthText +"&euro;, "+ languageJsStrings.total +": " + pricePerMonthText.replace(".", ",")+"&euro;*");

                        $("#summaryProductDescription").html(plans[i].PlanDescription);
                        $("#summaryProductImage").attr('src',".\\img\\shop\\"+plans[i].PlanImage);
                   } 
               }
            }
        },
        onStepChanging: function (event, currentIndex, newIndex){
            if (currentIndex > newIndex)
            {
                return true;
            }
       
            $("#orderFailedAlert").hide();
            if(currentIndex == 0)
            {
                if($("#numberOfPlayers").val() == "" || isNaN(parseInt($("#numberOfPlayers").val())))
                {
                    $("#orderFailedAlert").html(""+languageJsStrings.enterNumberOfPlayer);
                    $("#orderFailedAlert").show();
                    return false;
                }
                numberOfPlayerToOrder = $("#numberOfPlayers").val();
            }
            else if(currentIndex == 1)
            {
                if(selectedPlanId == -1){
                    $("#orderFailedAlert").html(""+languageJsStrings.selectPlan);
                    $("#orderFailedAlert").show();
                    return false;
                }
            }
            else if(currentIndex == 2)
            {
                var addressFields = ["firstname","secondName","street","houseNumber","place","postCode"];
                var returnDeliverBoolean = true;
                var returnBoolean = true;
                for (let i = 0; i < addressFields.length; i++) {
                    
                    if($("#"+addressFields[i]).val() == "")
                    {
                        $("#"+addressFields[i]).addClass("is-invalid");
                        $("#orderFailedAlert").html(""+languageJsStrings.fillDeliver);
                        $("#orderFailedAlert").show();
                        returnDeliverBoolean = false;
                    }
                }

                if ($('#deviantBillingAddress').prop('checked')) {
                    var billAddressFields = ["billfirstname","billSecondName","billstreet","billhouseNumber","billplace","billpostCode"];
                    
                    for (let i = 0; i < billAddressFields.length; i++) {
                        
                        if($("#"+billAddressFields[i]).val() == "")
                        {
                            $("#"+billAddressFields[i]).addClass("is-invalid");
                            $("#orderFailedAlert").html(""+languageJsStrings.fillBillAddresses);
                            $("#orderFailedAlert").show();
                            returnBoolean = false;
                        }
                    }
                }
                if(returnBoolean && returnDeliverBoolean)
                {
                    var addressInputs = $("#addressArea").find("input");
                    var hiddenAddressArea = document.getElementById("hiddenAddressArea");;
                    hiddenAddressArea.innerHTML = "";
                    for (let i = 0; i < addressInputs.length; i++) {
                        var cloneOfaddressInputs = addressInputs[i].cloneNode(true)
                        cloneOfaddressInputs.id = cloneOfaddressInputs.id + "_hidden";
                        cloneOfaddressInputs.name = cloneOfaddressInputs.name + "_hidden";
                        hiddenAddressArea.appendChild(cloneOfaddressInputs);  
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if(currentIndex == 3)
            {
                if(paymentid == -1){
                    $("#orderFailedAlert").html(""+languageJsStrings.selectPayment);
                    $("#orderFailedAlert").show();
                    return false;
                }
            }
            return true;
        },
        onFinishing: function (event, currentIndex)
        {
            if (!$('#acceptTerm').prop('checked')) {
                $("#acceptTerm").addClass("is-invalid");
                $("#orderFailedAlert").html(""+languageJsStrings.pleaseAcceptTerms);
                $("#orderFailedAlert").show();
                return false;
            }
            return true;
        },
        onFinished: function (event, currentIndex)
        {
            var form = $("#orderFrom");
            form.submit();
        }
    });

    $("#deviantBillingAddress").change(function (e) {
        if(this.checked) {
            $("#billingAddressArea").show();
            $("#billingAddress").val("true");
        }
        else
        {
            $("#billingAddressArea").hide();
            $("#billingAddress").val("false");
        }
    });

    $("#numberOfPlayers").keyup(function (e) { 

        if (/^\d+$/.test($(this).val())) {
            
        } 
        else 
        {
            console.log("char");
            this.value = 1;
        }
    });

    $("#numberOfPlayers").blur(function () { 
        if(parseInt(this.value) < 1)
        {
            decreaseNumberOfPlayer();
        }
    });


    $(".selectPlanEvent").click(function (e) { 
        $(".selectPlanEvent").removeClass("planIsSelected");
        e.preventDefault();
        $(this).addClass("planIsSelected");
        selectedPlanId = this.dataset.planid;
    });


    $(".paymentBox").click(function (e) { 
        $(".paymentBox").removeClass("planIsSelected");
        e.preventDefault();
        $(this).addClass("planIsSelected");
        paymentid = this.dataset.paymentid;
    });

    $("#privateCustomer").click(function (){
        $("#firstnameLabel").html(languageJsStrings.firstName);
        $("#secondNameLabel").html(languageJsStrings.secondName);

        $("#billfirstnameLabel").html(languageJsStrings.firstName);
        $("#billSecondNameLabel").html(languageJsStrings.secondName);
        $("#customerType").val("private");
    })

    $("#businessCustomer").click(function (){
        $("#firstnameLabel").html(languageJsStrings.company);
        $("#secondNameLabel").html(languageJsStrings.contactPerson);

        $("#billfirstnameLabel").html(languageJsStrings.company);
        $("#billSecondNameLabel").html(languageJsStrings.contactPerson);
        $("#customerType").val("business");
    })


});

function increaseNumberOfPlayer()
{
    var value = $("#numberOfPlayers").val();
    $("#numberOfPlayers").val(parseInt(value)+1);
}

function decreaseNumberOfPlayer()
{
    var value = $("#numberOfPlayers").val();
    value = parseInt(value)-1;
    if(value < 1 )
    {
        value = 1;
    }
    $("#numberOfPlayers").val(value);
}