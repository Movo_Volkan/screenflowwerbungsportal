function scrollToTop() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function checkPassword(str) {
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(str);
}

function getCookieValue(a) {
    const b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function uuidShort() {
    return 'xxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function setLanguageCookie(name,value)
{
  var d = new Date();
  d.setTime(d.getTime() + 10*365*24*60*60*1000);
  document.cookie = name+'='+value+';expires='+d.toGMTString()+';'; 
}

function setYoutubeCrossCookie()
{
    document.cookie = 'SameSite=None; Secure';
}

function stripHtml(html){
    // Create a new div element
    var temporalDivElement = document.createElement("div");
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
}

function checkRequiredInputFields(requiredArray)
{
    for (var i = 0; i < requiredArray.length; i++)
    {
        $("#"+requiredArray[i]).removeClass("is-invalid");
    }

    for (var i = 0; i < requiredArray.length; i++)
    {
        if($('#'+requiredArray[i]).val().length === 0)
        {
            $("#error_alert").html(languageJsStrings.selectMandatoryInput);
            $("#"+requiredArray[i]).addClass("is-invalid");
            $("#error_alert").fadeIn();
            return false;
        }
    }
    return true;
}

function disableButton(selector)
{
    $(""+selector).prop('disabled', true);
}

function enableButton(selector)
{
    $(""+selector).prop('disabled', true);
}

function hideElement(selector)
{
    $(""+selector).hide();
}

function showElement(selector)
{
    $(""+selector).show();
}

function reloadPage(timeInMilSec)
{
    if(timeInMilSec === undefined)
    {
        location.reload();
    }
    else
    {
        setTimeout(function(){ location.reload(); }, timeInMilSec);
    }
}

function showLadda(selector){
    const ladda = $(selector).ladda();
    ladda.ladda( 'start' );
}

function hideLadda(selector){
    const ladda = $(selector).ladda();
    ladda.ladda( 'stop' );
}

