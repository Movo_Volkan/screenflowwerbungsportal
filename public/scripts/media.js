function showMediaDeleteModal(index)
{
    $('#editMediaModal_'+index).modal('toggle');

    $('#editMediaModal_'+index).bind('hidden.bs.modal', function () {
        $('#deleteMedia_'+index).modal('toggle');
    });
}

function deleteMedia(index,mediaId,mediaType)
{
    $("#success_alert_modal_delete_media_"+index).fadeOut();
    $("#error_alert_modal_delete_image_"+index).fadeOut();
    var ladda = $( '#deleteMediaYesButton_'+index ).ladda();
    $("#deleteMediaYesButton_"+index).prop('disabled', true);
    $("#deleteMediaNoButton_"+index).prop('disabled', true);
    ladda.ladda( 'start' );

    $.post("removeMedium",
        {
            MediaId: mediaId,
            MediaType: mediaType
        })
        .done(function(result){
            ladda.ladda( 'stop' );
            $("#success_alert_modal_delete_media_"+index).html(result.message);
            $("#success_alert_modal_delete_media_"+index).fadeIn();
            $("#deleteMediaYesButton_"+index).prop('disabled', true);
            $("#deleteMediaNoButton_"+index).prop('disabled', true);
            $("#deleteMedia_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        })
        .fail(function(result) {
            ladda.ladda( 'stop' );
            $("#error_alert_modal_delete_media_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_delete_media_"+index).fadeIn();
            $("#deleteMediaYesButton_"+index).prop('disabled', false);
            $("#deleteMediaNoButton_"+index).prop('disabled', false);
            $("#deleteMedia_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        });
}

function editMedia(index,mediaId,mediaType)
{
    var ladda = $( '#saveMediaButton_'+index ).ladda();
    
    $("#editMediaName_"+index).removeClass("is-invalid");
    $("#success_alert_modal_edit_media_"+index).fadeOut();
    $("#error_alert_modal_edit_media_"+index).fadeOut();

    if($("#editMediaName_"+index).val().length < 3 || $("#editMediaName_"+index).val() == "" )
    {
        $("#error_alert_modal_edit_media_"+index).html(languageJsStrings.enterPictureName);
        $("#error_alert_modal_edit_media_"+index).fadeIn();
        $("#editMediaName_"+index).addClass("is-invalid");
    }
    else
    {
        $.post("editMedium",
        {
            MediaId: mediaId,
            MediaName: $("#editMediaName_"+index).val(),
            MediaDescription: $("#editMediaDescription_"+index).val(),
            MediaType: mediaType
        })
        .done(function(result){
            ladda.ladda( 'stop' );
            $("#success_alert_modal_edit_media_"+index).html(result.message);
            $("#success_alert_modal_edit_media_"+index).fadeIn();
            setTimeout(function(){ $("#success_alert_modal_edit_media_"+index).fadeOut(); }, 3000);
            $("#editMediaModal_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        })
        .fail(function(result) {;
            ladda.ladda( 'stop' );
            $("#error_alert_modal_edit_media_"+index).html(result.responseJSON.message);
            $("#error_alert_modal_edit_media_"+index).fadeIn();
            $("#editMediaModal_"+index).on('hidden.bs.modal', function () {
                location.reload();
            });
        });
    }
}


$('#uploadImage').on('hidden.bs.modal', function () {
    location.reload();
});

$('#uploadVideo').on('hidden.bs.modal', function () {
    location.reload();
});

$('.showVideoModal').on('hidden.bs.modal', function () {
    $("video").each(function () { 
        this.pause();
        this.currentTime = 0; 
    });
});

$("#searchMedia").keyup(function ()
{
    $("#noSearchResult").hide();
    var media = $(".mediaName");
    var emptyCounter = 0;
    for(var i = 0; i < media.length; i++ )
    {
        var html = stripHtml($(media[i]).html());
        html = html.toUpperCase();
        var filter = this.value.toUpperCase();
        
        if (html.indexOf(filter) > -1) 
        {
            media[i].parentElement.parentElement.parentElement.style.display = "";
        } 
        else 
        {
            media[i].parentElement.parentElement.parentElement.style.display = "none";
            emptyCounter++;
        }
    }

    if(emptyCounter == media.length)
    {
        $("#noSearchResult").show();
    }
});

function setupUppy() {  
    var uppy = Uppy.Core({
        restrictions: {
            allowedFileTypes: ['image/*', 'video/*']
        }
    });
    uppy.use(Uppy.Dashboard, {
        inline: true,
        target: '#drag-drop-area',
        proudlyDisplayPoweredByUppy:false,
        allowMultipleUploads: true,
        bundle: true,
        locale: Uppy.locales.de_DE,
        width: '100%',
        height: '50%',
        waitForThubnailsBeforeUpload:true
    })
    .use(Uppy.XHRUpload,
        {
            endpoint: '/uploadmedia'
    });
    
    uppy.on('complete', (result) => {
        console.log('Upload complete! We’ve uploaded these files:', result.successful)
    });

    uppy.on('upload-success', (file, response) => {
        response.status // HTTP status code
        response.body   // extracted response data
        if(file.type.includes('video'))
        {
            let videoId = file.id;
            var srcBlob = $('div[id*="' + videoId + '"]').find('.uppy-Dashboard-Item-previewImg')[0].src;
            setThumbnail(response.body.id,srcBlob)
        }
    });

    uppy.on('file-added', (file) => {
        uppy.setMeta(
            {
                category: document.getElementById('kategorien').options[document.getElementById('kategorien').selectedIndex].text,
                tags: document.getElementById('tags').value,
                ageCategory: document.getElementById('altersgruppen').options[document.getElementById('altersgruppen').selectedIndex].value,
                gender: document.getElementById('geschlecht').options[document.getElementById('geschlecht').selectedIndex].value
            }
        )
        if(file.type.includes('video'))
        {
            const thumbnailCount = 1;
            const thumbnails = new VideoThumbnails({
                count : thumbnailCount,
                maxWidth : 400,
                maxHeight : 400
            });
            //Captured a thumb
            thumbnails.on('capture', function(image) {
                let videoId = file.id.split('/').slice(-1).pop();
                $('div[id*="' + videoId + '"]')
                    .find('.uppy-Dashboard-Item-previewInnerWrap')
                    .html('<img class="uppy-Dashboard-Item-previewImg" src="'+image+'"/>');
            });
            thumbnails.capture(file.data);
        }
    });
}

function setThumbnail(mediaId,thumbnail){
    $.post("/setthumbnail",
    {
        MediaId: mediaId,
        Thumbnail:thumbnail
    },
    function(result)
    {   

    });
}

function loadVideoContent(element) {
    var targetElement = $(element.dataset.target).find(".videoContainer");
    targetElement = targetElement[0];
    if(targetElement.children.length <= 0)
    {
        $(targetElement).html("");
        var videoSrc = videoURLSArray[element.dataset.index].videoURL;
        var newVideo = document.createElement("VIDEO");
        newVideo.src = videoSrc+"#t=0.1";
        targetElement.appendChild(newVideo);
    }
}