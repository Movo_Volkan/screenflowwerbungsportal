var sortableScreenLine ="";
var sortableScreenItems ="";
function createPlaylist()
{
  var ladda = $( '#addPlaylistYesButton' ).ladda();
  var playlistName = $("#newPlaylistNameId").val();

  var screenOrientation = "";
  if($("#screenOrientationLandscape").is(':checked'))
  {
    screenOrientation = "1";
  }
  else if($("#screenOrientationPortrait").is(':checked'))
  {
    screenOrientation = "0";
  }

  $("#error_alert_modal_add_playlist").fadeOut();
  $("#success_alert_modal_add_playlist").fadeOut();
  if(playlistName.length < 3 || playlistName == "")
  {
    $("#error_alert_modal_add_playlist").html(languageJsStrings.enterPlaylistname);
    $("#error_alert_modal_add_playlist").fadeIn();
  }
  else
  {
    ladda.ladda('start');
    $.post("playlist/create", {
      PlaylistName: $("#newPlaylistNameId").val(),
      PlaylistDescription: $("#newPlaylistDescription").val(),
      ScreenOrientation: screenOrientation
    })
    .done(function(result){
      $("#success_alert_modal_add_playlist").html(result.message);
      window.location = "playlisteditor/"+result.playlistId;
      $("#success_alert_modal_add_playlist").fadeIn();
      $("#addPlaylist").on('hidden.bs.modal', function () {
        location.reload();
      });
      setTimeout(function(){ $("#success_alert_modal_add_playlist").fadeOut(); $("#createPlaylist").prop('disabled', false);}, 3000);
    })
    .fail(function(result) {
      ladda.ladda('stop');
      $("#error_alert_modal_add_playlist").html(result.responseJSON.message);
      $("#error_alert_modal_add_playlist").fadeIn();
    });
  }
}

function deletePlaylist(index,playlistId)
{
  var ladda = $( '#deletePlaylistYesButton_'+index ).ladda();
  $("#success_alert_modal_delete_playlist_"+index).fadeOut();
  $("#error_alert_modal_delete_playlist_"+index).fadeOut();

  $("#deletePlaylistYesButton_"+index).prop('disabled', true);
  $("#deletePlaylistNoButton_"+index).prop('disabled', true);
  ladda.ladda('start');
  $.post("playlist/remove",
  {
      PlaylistId: playlistId
  })
  .done(function(result){
    ladda.ladda('stop');
    $("#success_alert_modal_delete_playlist_"+index).html(result.message);
    $("#success_alert_modal_delete_playlist_"+index).fadeIn();
    $("#deletePlaylistYesButton_"+index).prop('disabled', true);
    $("#deletePlaylistNoButton_"+index).prop('disabled', true);
    $("#deletePlaylist_"+index).on('hidden.bs.modal', function () {
      location.reload();
    });
  })
  .fail(function(result) {
    ladda.ladda('stop');
    $("#error_alert_modal_delete_playlist_"+index).html(result.responseJSON.message);
    $("#error_alert_modal_delete_playlist_"+index).fadeIn();
    $("#deletePlaylistYesButton_"+index).prop('disabled', false);
    $("#deletePlaylistNoButton_"+index).prop('disabled', false);
    $("#deletePlaylist_"+index).on('hidden.bs.modal', function () {
      location.reload();
    });
  });
}

function deletePlaylistScreenItem(element)
{
  element.parentElement.parentElement.parentElement.remove();
  setItemPosition();
}

function addScreenToPlaylist(playlistId)
{
  $("#error_alert").fadeOut();
  $("#success_alert").fadeOut();
  if(playlistId == "")
  {
    $("#error_alert").html(languageJsStrings.playlistIdNotFound);
    $("#error_alert").fadeIn();
  }
  else
  {
    $.post("playlisteditor/addscreen", {
      ScreenId: $("#screenList").val(),
      PlaylistId: playlistId,
    })
    .done(function(result){
      location.reload();
    })
    .fail(function(result) {
      $("#error_alert").html(result.responseJSON.message);
      $("#error_alert").fadeIn();
    });
  }
}

function removeScreenFromPlaylist(element,playlistHasScreenId)
{
  $("#error_alert").fadeOut();
  $("#success_alert").fadeOut();
  if(playlistHasScreenId == "")
  {
    $("#error_alert").html(languageJsStrings.playlistIdNotFound);
    $("#error_alert").fadeIn();
  }
  else
  {
    $.post("playlistEditor/removePlaylistScreenItem", {
      PlaylistHasScreenId: playlistHasScreenId,
    })
    .done(function(result){
      element.parentElement.parentElement.parentElement.remove();
      setItemPosition();
      $("#success_alert").html(result.message);
      $("#success_alert").fadeIn();
      setTimeout(function(){ $("#success_alert").fadeOut();}, 1500);
    })
    .fail(function(result) {
      $("#error_alert").html(result.responseJSON.message);
      $("#error_alert").fadeIn();
    });
  }
}

function saveScreenPositions()
{
  $.post("playlistEditor/removePlaylistScreenItem", {
    PlaylistHasScreenId: playlistHasScreenId,
  })
      .done(function(result){
        element.parentElement.parentElement.parentElement.remove();
        setItemPosition();
        $("#success_alert").html(result.message);
        $("#success_alert").fadeIn();
        setTimeout(function(){ $("#success_alert").fadeOut();}, 1500);
      })
      .fail(function(result) {
        $("#error_alert").html(result.responseJSON.message);
        $("#error_alert").fadeIn();
      });
}

function savePlaylistScreenItems()
{
  $("#error_alert").fadeOut();
  $("#success_alert").fadeOut();
  var sortedListTr = $("#playlistHasScreenItems").find("tr");
  var sortedListDuration = $("#playlistHasScreenItems").find(".duration");
  var sortedListExpireCheckbox = $("#playlistHasScreenItems").find(".expireCheckbox");
  var sortedListExpire = $("#playlistHasScreenItems").find(".expire");
  
  
  var newSortedList = [];

  for (let i = 0; i < sortedListTr.length; i++) {
    
    if(sortedListExpireCheckbox[i].checked)
    {
      newSortedList.push([sortedListTr[i].dataset.playlistHasScreenId,sortedListDuration[i].value,sortedListExpire[i].value]);
    }
    else
    {
      newSortedList.push([sortedListTr[i].dataset.playlistHasScreenId,sortedListDuration[i].value,""]);
    }
    
  }

  if(newSortedList.length <= 0)
  {
    $("#error_alert").html(languageJsStrings.noScreensInPlaylist);
    $("#error_alert").fadeIn();
  }
  else if(playlistName.length < 3 || playlistName == "")
  {
    $("#error_alert").html(languageJsStrings.enterPlaylistname);
    $("#error_alert").fadeIn();
    $("#screenName").addClass("is-invalid");
  }
  else
  {
    $.post("playlistEditor/savePlaylistScreenItems", {
      SortedList: newSortedList,
      PlaylistId: $("#PlaylistId").val(),
      PlaylistName:$("#playlistName").val(),
      PlaylistDescription:$("#playlistDescription").val()
    })
    .done(function(result){
      $("#success_alert").html(result.message);
      $("#success_alert").fadeIn();
      setTimeout(function(){ $("#success_alert").fadeOut();}, 1500);
    })
    .fail(function(result) {

      $("#error_alert").html(result.responseJSON.message);
      $("#error_alert").fadeIn();
    });
  }
}

function showExpireInput(element)
{
  var expireElement = $(element.parentElement).find(".expire")[0];

  if (element.checked == true)
  {
    $(expireElement).show();
  } 
  else 
  {
    $(expireElement).hide();
  }
}

function preventKeyUp(element)
{
  var defaultValue = element.defaultValue;
  element.value = defaultValue;
}

function setRowExpired()
{
  var tableRow = $("#playlistHasScreenItems").find(".expireDate");

  for (var i = 0; i < tableRow.length; i++) {
    var date = moment(tableRow[i].value, "DD.MM.YYYY hh:mm").toDate();
    var now = new Date();
    if (date < now) {
      tableRow[i].style.backgroundColor = "red";
    }
    else
    {
      tableRow[i].style.backgroundColor = "";
    }
  }
}