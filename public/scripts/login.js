function checkRegisterForm()
{
    
    $("#success_alert").fadeOut();
    $("#error_alert").fadeOut();

    $("#serverMessageSuccess").fadeOut();
    $("#serverMessageError").fadeOut();
    
    
    var firstname = $("#Firstname").val();
    var secondname =$("#secondname").val();
    var email =$("#Email").val();
    var password =$("#Password").val();
    var agb = $('#AGB').is(':checked');


    if(firstname == "" && firstname.length < 3 )
    {
        $("#error_alert").html("Bitte trage Deinen Namen ein.");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(secondname == "" && secondname.length < 3)
    {
        $("#error_alert").html("Bitte trage Deinen Vornamen ein.");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(email == "" && email.length < 3)
    {
        $("#error_alert").html("Bitte trage eine gültige E-Mail-Adresse ein.");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(password == "" && password.length < 6)
    {
        $("#error_alert").html("Bitte trage dein gewünschtes Passwort ein. Mindestens 6 Zeichen.");
        $("#error_alert").fadeIn();
        return false;
    }
    else if(!agb)
    {
        $("#error_alert").html("Bitte akzeptiere die Allgemeine Geschäftsbedingungen.");
        $("#error_alert").fadeIn();
        return false;
    }

    return true;
    

}