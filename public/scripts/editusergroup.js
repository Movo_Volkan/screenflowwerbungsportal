function addUserToUserGroup()
{
    var ladda = $( '#addUserToUserGroup' ).ladda();
    ladda.ladda( 'start' );
    if ($("#newUserForGroupEmail").val().length < 3 || $("#newUserForGroupEmail").val() == "")
    {
        $("#error_alert_modal").html(languageJsStrings.enterGroupName);
        $("#error_alert_modal").fadeIn();
        $("#newUserForGroupEmail").addClass("is-invalid");
    }
    else
    {
        $.post("./functions/usergroup.php?Function=AddUserGroup",
        {
            UserGroupEMail: $("#newUserForGroupEmail").val(),
            UserGroupID: $("#GroupID").val(),
            UserGroupDescription: $("#newUserForGroupDescription").val(),
            UserGroupeRoleID: $("#roleList").val()
        },
        function(result)
        {
            try
            {
                var jsonResult = JSON.parse(result);
                if(jsonResult.status == "successfull")
                {
                    $("#success_alert_modal").html(jsonResult.message);
                    $("#success_alert_modal").fadeIn();
                    setTimeout(function(){ $("#success_alert_modal").fadeOut(); }, 3000);
                }
                else if(jsonResult.status == "error")
                {
                    $("#error_alert_modal").html(jsonResult.message);
                    $("#error_alert_modal").fadeIn();
                }
                $("#addUserGroup").on('hidden.bs.modal', function () {
                    location.reload();
                });
                ladda.ladda( 'stop' );
            }
            catch (exception) 
            {
                $("#error_alert_modal").html(exception + "Result: " + result);
                $("#error_alert_modal").fadeIn();
                $("#addUserGroup").on('hidden.bs.modal', function () {
                    location.reload();
                });
            }
        });
    }
}