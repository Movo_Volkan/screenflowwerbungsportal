let screens = [];
let playlist = [];

class Screen {
    constructor(id, name, position, duration, expired, thumbnail) {

        this.id = id;

        this.playlistElementId = this.uuidv4();
        this.name = name;
        this.position = position;
        this.duration = duration;
        this.expired = expired;
        this.thumbnail = thumbnail;
        console.log("Screen Element with ID: " + this.id + " created.");
    }

    getId() {
        return this.id;
    }

    getPlaylistElementId() {
        return this.playlistElementId;
    }

    getPosition() {
        return this.position;
    }

    setPosition(position) {
        this.position = position
    }

    getDuration() {
        return this.duration;
    }

    setDuration(duration) {
        this.duration = duration;
    }

    getExpired() {
        return this.expired;
    }

    setExpired(expired) {
        this.expired = expired;
    }

    getThumbnail() {
        return this.thumbnail;
    }

    setThumbnail(thumbnail) {
        this.thumbnail = thumbnail;
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }
}

class ScreenInsertElement {
    constructor(id, name, position, duration, expired, thumbnail) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.duration = duration;
        this.expired = expired;
        this.thumbnail = thumbnail;
        console.log("Screen Element with ID: " + this.id + " created.");
    }

    getId() {
        return this.id;
    }

    getPosition() {
        return this.position;
    }

    setPosition(position) {
        this.position = position
    }

    getDuration() {
        return this.duration;
    }

    setDuration(duration) {
        this.duration = duration;
    }

    getExpired() {
        return this.expired;
    }

    setExpired(expired) {
        this.expired = expired;
    }

    getThumbnail() {
        return this.thumbnail;
    }

    setThumbnail(thumbnail) {
        this.thumbnail = thumbnail;
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }
}

function setDefaultEvents() {

    let addScreenToPlaylistEvent = function(arr, callback) {
        arr.push = function(e) {
            Array.prototype.push.call(arr, e);
            callback(arr);
        };
    };

    addScreenToPlaylistEvent(getPlaylist(), function(newArray) {
        
    });

    let addScreenElement = function(arr, callback) {
        arr.push = function(e) {
            Array.prototype.push.call(arr, e);
            callback();
        };
    };

    addScreenElement(getScreens(), function() {
        refreshScreenElements();
    });
}

function onInit() {

    loadScreens();
}

function loadScreens() {

    exampleData();

}

function refreshScreenElements() {
    
    let screenElementPanel = document.getElementById("screenElementPanel");
  
    screenElementPanel.innerHTML = "";

    getScreens().forEach((screen) => {
        let screenDiv = document.createElement("div");
        screenDiv.id = screen.getId();
        screenDiv.classList.add("screenElement");
        screenDiv.classList.add("col-1");
        screenDiv.classList.add("tooltipScreen");
        screenDiv.setAttribute('draggable', true);
        screenDiv.addEventListener('dragstart', function(ev) {drag(ev)}, false);
        screenDiv.owner = screen;

        let tooltipSpan = document.createElement("span");
        tooltipSpan.innerHTML= screen.name;
        tooltipSpan.classList.add("tooltiptext");
        screenDiv.appendChild(tooltipSpan);
        
        let screenThumbnail = document.createElement("img");
        screenThumbnail.src = screen.getThumbnail();
        screenDiv.appendChild(screenThumbnail);
        screenElementPanel.appendChild(screenDiv);
    });
}

function allowDrop(ev) {
    ev.preventDefault();
}
  
function drag(ev) {
    ev.dataTransfer.setData("text", ev.currentTarget.id);
}
  
function drop(ev) {
    
    if(ev.dataTransfer === undefined) {
        // Double click
        var data = ev;
    }
    else {
        //Drag and drop
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
    }
    
    if(data != null && data != "")
    {
        let screen = document.getElementById(data).owner;
        addScreenToTrack(screen.id,screen.name,getPlaylist().length+1,5,screen.expired,screen.thumbnail);
    }
    else
    {
        console.log("Element not Found.")
    }
}

function bindElementEvents(element) {

    
}

$(document).ready(function () {

    setDefaultEvents();
    onInit();

    $("#playlistTrack").sortable({
        placeholder: "ui-state-highlight",
        cursor: 'pointer',
    }); 

    $( "#playlistTrack" ).sortable({
        stop: function( event, ui ) {
            refreshScreenElementPositions();
        }
    });
});

function exampleData() {

    screenMapper('{\r\n    \"screens\":\r\n    [\r\n        {\r\n            \"id\": \"1\",\r\n            \"name\": \"Name1\",\r\n            \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n        },\r\n        {\r\n            \"id\": \"2\",\r\n            \"name\": \"Name2\",\r\n            \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n        },\r\n        {\r\n            \"id\": \"3\",\r\n            \"name\": \"Name3\",\r\n            \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n        },\r\n        {\r\n            \"id\": \"4\",\r\n            \"name\": \"Name4\",\r\n            \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n        }\r\n\t]\r\n}');
    showEditor();
    playlistTrackMapper('{\r\n    \"playlist\": {\r\n        \"screens\":[\r\n            {\r\n                \"id\": \"9c0f0a2c-55b4-464f-a529-7f96d79c0041\",\r\n                \"screenId\":\"9c0f0a2c-55b4-464f-a529-7f96d79c0064\",\r\n                \"name\": \"name1\",\r\n                \"position\": 1,\r\n                \"duration\": 180,\r\n                \"expired\" : \"2021-05-09 11:10:00\",\r\n                \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n            },\r\n            {\r\n                \"id\": \"9c0f0a2c-55b4-464f-a529-7f96d79c0042\",\r\n                \"screenId\":\"9c0f0a2c-55b4-464f-a529-7f96d79c0044\", \r\n                \"name\": \"name2\",\r\n                \"position\": 2,\r\n                \"duration\": 180,\r\n                \"expired\" : \"2021-05-09 11:10:00\",\r\n                \"thumbnail\": \"https:\/\/homepages.cae.wisc.edu\/~ece533\/images\/airplane.png\"\r\n            }\r\n        ]\r\n    }\r\n}')
}

function screenMapper(jsonString) {
    const jsonObj = JSON.parse(jsonString);
    jsonObj.screens.forEach (screen => {
        addScreen(screen.id,screen.name,screen.thumbnail);
    });
    
}

function playlistTrackMapper(jsonString) {
    const jsonObj = JSON.parse(jsonString);
    jsonObj.playlist.screens.forEach (screen => {
        addScreenToTrack(screen.screenId,screen.name,screen.position,screen.duration,screen.expired,screen.thumbnail)
    });
}

function addScreenToTrack(id, name, position, duration, expired, thumbnail) {

    var dateFormatVariable;
    if(expired == null)
    {
        dateFormatVariable = null;
    }
    else
    {
        dateFormatVariable = dateFormat(moment(expired, "YYYY-MM-DD hh:mm",'de').toDate());
    }
    let screen = new Screen(id, name, position, duration, dateFormatVariable, thumbnail);
    let playlistTrack = document.getElementById("playlistTrack");
    let draggableDiv = document.createElement("div");
    draggableDiv.id = screen.getPlaylistElementId();
    draggableDiv.owner = screen;
    draggableDiv.classList.add("ui-state-default");

    let trashButton = document.createElement("button");
    trashButton.innerHTML = '<i class="fa fa-trash">';
    trashButton.classList.add("btn");
    trashButton.classList.add("btn-primary");
    trashButton.classList.add("trashButton");

    trashButton.addEventListener("click", (event) => {
        if(event.originalTarget.parentElement.owner !== undefined)
        {
            var playlistIdElement = event.originalTarget.parentElement.owner.getPlaylistElementId();
        }
        else if(event.originalTarget.parentElement.parentElement.owner != undefined) {
            var playlistIdElement = event.originalTarget.parentElement.parentElement.owner.getPlaylistElementId();
        }
        
        document.getElementById(playlistIdElement).remove();
        let index = getPlaylist().findIndex((element) => {
            return element.getPlaylistElementId() === playlistIdElement;
        });
        getPlaylist().splice(index, 1);
        refreshScreenElementPositions();
    });

    draggableDiv.appendChild(trashButton);

    let trackImg = document.createElement("img");
    trackImg.src = screen.getThumbnail();
    draggableDiv.appendChild(trackImg);

    //Duration 
    let durationInputLable = document.createElement("label");
    durationInputLable.htmlFor = screen.getPlaylistElementId() + "-durationInput";
    durationInputLable.innerHTML = languageJsStrings.durationSec;
    draggableDiv.appendChild(durationInputLable);

    let durationInput = document.createElement("input");
    durationInput.type = "text";
    durationInput.value = screen.getDuration();
    durationInput.classList.add("durationInput");
    durationInput.addEventListener("input", (event)  => {
        event.originalTarget.parentElement.owner.setDuration(event.target.value);
    });
    draggableDiv.appendChild(durationInput);

    //Expired Checkbox
    let expiredCheckbox = document.createElement("input");
    expiredCheckbox.id = screen.getPlaylistElementId() + "-expiredCheckbox";
    expiredCheckbox.type = "checkbox";
    expiredCheckbox.addEventListener('change', (event) => {
        if (event.currentTarget.checked) {
            console.log(event.currentTarget.parentElement.owner.getPlaylistElementId());
            let expiredInput = document.getElementById(event.currentTarget.parentElement.owner.getPlaylistElementId() + "-expiredInput");
            expiredInput.style.display = "";

            let expiredInputLabel = document.getElementById(event.currentTarget.parentElement.owner.getPlaylistElementId() + "-expiredInputLabel");
            expiredInputLabel.style.display = "";
        } 
        else {
            let expiredInput = document.getElementById(event.currentTarget.parentElement.owner.getPlaylistElementId() + "-expiredInput");
            expiredInput.style.display = "none";
            expiredInput.value = "";
            event.currentTarget.parentElement.owner.setExpired(null);
            let expiredInputLabel = document.getElementById(event.currentTarget.parentElement.owner.getPlaylistElementId() + "-expiredInputLabel");
            expiredInputLabel.style.display = "none";
        }
        expandPlaylistTrackToBottom();
      })
    draggableDiv.appendChild(expiredCheckbox);

    let expiredCheckboxLable = document.createElement("label");
    expiredCheckboxLable.classList.add("expired-checkbox-label")
    expiredCheckboxLable.htmlFor = screen.getPlaylistElementId() + "-expiredCheckbox";
    expiredCheckboxLable.innerHTML = languageJsStrings.expireScreen;
    draggableDiv.appendChild(expiredCheckboxLable);

    //BR
    var br = document.createElement("br");
    draggableDiv.appendChild(br);

    //Expired
    let expiredInputLabel = document.createElement("label");
    expiredInputLabel.htmlFor = screen.getPlaylistElementId() + "-expiredInput";
    expiredInputLabel.id = screen.getPlaylistElementId() + "-expiredInputLabel"
    expiredInputLabel.innerHTML = languageJsStrings.expireScreen;
    draggableDiv.appendChild(expiredInputLabel);

    let expiredInput = document.createElement("input");
    expiredInput.id = screen.getPlaylistElementId() + "-expiredInput";
    expiredInput.type = "text";
    
    expiredInput.classList.add("expiredInput");
    expiredInput.readOnly = true;

    expiredInput.addEventListener("input", (event) => {
        markExpiredInputs();
    });

    draggableDiv.appendChild(expiredInput);

    if(screen.getExpired() == null || screen.getExpired() == "") {
        expiredInput.style.display = "none";
        expiredInputLabel.style.display = "none";
    }
    else {
        expiredCheckbox.checked = true;
        expiredInput.value = screen.getExpired();
    }
   
    playlistTrack.appendChild(draggableDiv);
    getPlaylist().push(screen);
    refreshScreenElementPositions();

    $(expiredInput).datetimepicker({
        isRTL: false,
        format: 'dd.mm.yyyy hh:ii',
        todayBtn: true,
        language: "de",
        weekStart: 1,
        minDate: 0,
    }).on("changeDate", function(event) {
        console.log("r")
        event.currentTarget.parentElement.owner.setExpired(dateFormat(moment(event.date, "YYYY-MM-DD hh:mm",'de').toDate()));
        markExpiredInputs();
    });

    markExpiredInputs();
    expandPlaylistTrackToBottom();
}

function getLayoutJsonPayload(title,description) {

    return saveMapper(title,description);
}

function save() { 

    var title = document.getElementById("titleInput");
    removeInvalid(title);

    var description = document.getElementById("descriptionInput");
    removeInvalid(description);

    if(title.value === "") {
        showDangerAlert("Please enter a title.");
        addInvalid(title);
        return false;
    }
    else if(description.value === "") {
        showDangerAlert("Please enter a description.");
        addInvalid(description);
        return false;
    }
    else if(getPlaylist().length === 0) {
        showDangerAlert("Please add a screen.");
        return false;
    }
    getLayoutJsonPayload(title.value,description.value);
}

function saveMapper(title, description) {
    const getCircularReplacer = () => {
    const seen = new WeakSet;
    return (key, value) => {
            if (typeof value === "object" && value !== null) {
                if (seen.has(value)) {
                    return;
                }
                seen.add(value);
            }
            return value;
        };
    };

    let playlistArray = JSON.parse(JSON.stringify(getPlaylist(), getCircularReplacer()));

    var layout = {
        "title": title,
        "description": description,
        "playlist": playlistArray
    } 
    console.log(layout);
    return layout;
}

function removeElement() {

    let widgetElement = getWidgets().find(element => element.id == selectedElement.id);
    document.getElementById(widgetElement.id).remove();
    for (let i = 0; i < getWidgets().length; i++) {
        if(getWidgets()[i].id == widgetElement.id) {
            getWidgets().splice(i, 1);
            break;
        }
    }

    if(widgetElement.constructor.name == "TickerWidget") {
        widgetElement.clearSlideEffect();
    }

    console.log("Widget with ID: " + widgetElement.id + " removed.");
    refreshLayerPanel();
    deselectAll();
}

function refreshScreenElementPositions() { 
    let playlistElements = document.getElementById("playlistTrack").children;
    for (let i = 0; i < playlistElements.length; i++) {
        playlistElements[i].owner.setPosition(i+1)
    }
}

/* UI-Functions */
function showDangerAlert(message) {

    document.getElementById("alertDanger").classList.remove("alert-hide");
    document.getElementById("alertDanger").innerHTML = message;

    setTimeout(function() {
        document.getElementById("alertDanger").classList.add("alert-hide");
    }, 5000);
}

function showSuccessAlert(message) {
    document.getElementById("alertSuccess").classList.remove("alert-hide");
    document.getElementById("alertSuccess").innerHTML = message;

    setTimeout(function() {
        document.getElementById("alertSuccess").classList.add("alert-hide");
    }, 5000);
}

function addValid(element) {
    element.classList.add("is-valid");
}

function removeValid(element) {
    element.classList.add("is-valid");
}

function addInvalid(element) {
    element.classList.add("is-invalid");
}

function removeInvalid(element) {
    element.classList.remove("is-invalid");
}

function showEditor()
{
    document.getElementById("loadingScreen").style.display = "none";
}

function addScreen(id, name, thumbnail)
{
    getScreens().push(new ScreenInsertElement(id, name, -1, -1, null, thumbnail));
}

function expandPlaylistTrackToBottom() {
    let playlistElements = document.getElementsByClassName("expiredInput");

    for (let i = 0; i < playlistElements.length; i++) {
        if(window.getComputedStyle(playlistElements[i]).display !== "none")
        {
            let playlistTrack = document.getElementById("playlistTrack");
            playlistTrack.classList.add("expand-playlist-track-bottom");
            return;
        }
    }
    playlistTrack.classList.remove("expand-playlist-track-bottom");
}

function dateFormat(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    var n = date.getMinutes();
    var h = date.getHours();
    return '' + (d <= 9 ? '0' + d : d) + '.' + (m<=9 ? '0' + m : m) + '.' + y + ' ' + (h<=9 ? '0' + h : h) + ':' + (n<=9 ? '0' + n : n);
}

function markExpiredInputs()
{
    let expiredInputs = document.getElementsByClassName("expiredInput");

    for (var i = 0; i < expiredInputs.length; i++) {
        var date = moment(expiredInputs[i].value, "DD.MM.YYYY hh:mm").toDate();
        var now = new Date();
        if (date < now) {
            expiredInputs[i].style.backgroundColor = "red";
        }
        else
        {
            expiredInputs[i].style.backgroundColor = "";
        }
    }
}

/**Getter-Setter */

function getScreens() {
    return screens;
}

function getPlaylist() {
    return playlist;
}