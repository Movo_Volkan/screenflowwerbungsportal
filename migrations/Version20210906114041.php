<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210906114041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE media (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, type VARCHAR(10) NOT NULL, created DATETIME NOT NULL, thumbnail LONGBLOB DEFAULT NULL, size BIGINT NOT NULL, unique_file_name VARCHAR(255) NOT NULL, media_category VARCHAR(255) NOT NULL, tags VARCHAR(255) NOT NULL, age_category VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, INDEX IDX_6A2CA10CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(254) NOT NULL, is_active TINYINT(1) NOT NULL, password VARCHAR(255) NOT NULL, salutation VARCHAR(45) DEFAULT NULL, firstname VARCHAR(255) NOT NULL, secondname VARCHAR(255) NOT NULL, profile_picture LONGBLOB DEFAULT NULL, register_code LONGTEXT DEFAULT NULL, activate TINYINT(1) NOT NULL, password_reset_code LONGTEXT DEFAULT NULL, password_reset_code_time DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, first_login TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE version (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', nr VARCHAR(45) NOT NULL, description VARCHAR(255) NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10CA76ED395');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE version');
    }
}
